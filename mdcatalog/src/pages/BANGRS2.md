---
title: BANGRS2
date: "2021-12-16"
---

# BANGRS2

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
3. `IHIGHER`	Season 4 (days 301-present_ July 2021-present)/348d Sony Ericsson K750i - Higher.mp3
4. `_SIGNAL`	Season 3 (days 182-300_ March 2021-July 2021)/199n Kyocera Kona - Poppin_ Signal.ogg
5. `0AMINOR`	Season 3 (days 182-300_ March 2021-July 2021)/197n LG S5100 - A minor.wav
6. `CEPTRUM`	Season 3 (days 182-300_ March 2021-July 2021)/265n Android 2.3 Gingerbread - Sceptrum.ogg
7. `000MOON`	Season 4 (days 301-present_ July 2021-present)/391d Motorola A1000 - Moon.mp3
8. `LAQRIFF`	Season 4 (days 301-present_ July 2021-present)/342d Motorola Q - Riff.mp3
9. `0GROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/149d Motorola A1000 - Groove.mp3
10. `LLREMIX`	Season 1 (days 1-91_ September 2020-December 2020)/63 Discord - Incoming Call (Remix).mp3
11. `HTSWING`	Season 4 (days 301-present_ July 2021-present)/365n Samsung Galaxy S9 - Midnight Swing.ogg
12. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
13. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
14. `EPOLISH`	Season 3 (days 182-300_ March 2021-July 2021)/265d HTC Ozone - Polish.wma
15. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
16. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
17. `QBOOGIE`	Season 4 (days 301-present_ July 2021-present)/383n Windows Mobile (downloadable) - BBQ Boogie.wma
18. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
19. `TARTUP3`	Season 4 (days 301-present_ July 2021-present)/380dd KDE 3.3 - Startup 3.ogg
20. `JUPITER`	Season 3 (days 182-300_ March 2021-July 2021)/270d Alcatel OT-660 - Jupiter.mp3
21. `CHINESE`	Season 3 (days 182-300_ March 2021-July 2021)/206n1 Nokia Tune Remake Audiodraft Sound Design Contest - NOKIA CHINESE.mp3
22. `5RUNWAY`	Season 4 (days 301-present_ July 2021-present)/350d Nokia N85 - Runway.wma
23. `10GROWL`	Season 2 (days 92-181_ December 2020-March 2021)/107n Android 1.0 - Growl.ogg
24. `CSLEAZY`	Season 1 (days 1-91_ September 2020-December 2020)/10 Motorola E1000 - Mcsleazy.mp3
25. `CEPARTY`	Season 1 (days 1-91_ September 2020-December 2020)/72 Samsung Galaxy S6 - Dance Party.ogg
26. `NE7PURE`	Season 1 (days 1-91_ September 2020-December 2020)/08 Windows Phone 7 - Pure.wma
27. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
28. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
29. `95GLINT`	Season 3 (days 182-300_ March 2021-July 2021)/216n Nokia N95 - Glint.aac
30. `ANDWINE`	Season 2 (days 92-181_ December 2020-March 2021)/108n LG KG280 - Cigar And Wine.flac
31. `BAGITUP`	Season 1 (days 1-91_ September 2020-December 2020)/11 BenQ-Siemens S68 - Bag it up.wav
32. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
33. `310KICK`	Season 4 (days 301-present_ July 2021-present)/340d Nokia 3310 - Kick.wma
34. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
35. `LITHAZE`	Season 4 (days 301-present_ July 2021-present)/338d Motorola E815 - Moonlit Haze.wav
36. `LLATION`	Season 3 (days 182-300_ March 2021-July 2021)/240n Motorola ROKR EM35 - Oscillation.mp3
37. `GTONE17`	Season 3 (days 182-300_ March 2021-July 2021)/277n LG enV2 - Ringtone 17.wav
38. `K3SHINE`	Season 2 (days 92-181_ December 2020-March 2021)/164d Motorola KRZR K3 - Shine.mp3
39. `ECLOUDS`	Season 4 (days 301-present_ July 2021-present)/310d Sony Ericsson P1i - Into the clouds.mp3
40. `OUNGING`	Season 3 (days 182-300_ March 2021-July 2021)/276d Nokia 7370 - Lounging.wav
41. `2PEPPER`	Season 3 (days 182-300_ March 2021-July 2021)/205n Nokia N72 - Pepper.aac
42. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380de Mandriva Linux 2006 - Startup.wav
43. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
44. `NKY_ALL`	Season 4 (days 301-present_ July 2021-present)/302d Android 1.5 Cupcake - Funk Y_all.wav
45. `NGARING`	Season 2 (days 92-181_ December 2020-March 2021)/172n Samsung Galaxy Advance - Ring A Ring.ogg
46. `UNNYDAY`	Season 2 (days 92-181_ December 2020-March 2021)/103d Samsung C5212 - Sunny day.mp3
47. `UNLEASH`	Season 2 (days 92-181_ December 2020-March 2021)/110d Motorola Q9c - Unleash.mp3
48. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
49. `AMBIENT`	Season 3 (days 182-300_ March 2021-July 2021)/292n Motorola RAZR V3i - Ambient.mp3
50. `LATINUM`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
51. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
52. `NODANCE`	Season 3 (days 182-300_ March 2021-July 2021)/192d Samsung SGH-P250 - Techno dance.mp3
53. `OCOLATE`	Season 2 (days 92-181_ December 2020-March 2021)/115n Windows Mobile Ring Tone Pack 3 - Hot Chocolate.wma
54. `RW5COOL`	Season 3 (days 182-300_ March 2021-July 2021)/217n Motorola ROKR W5 - Cool.mp3
55. `ELSGOOD`	Season 4 (days 301-present_ July 2021-present)/320n Samsung Galaxy S20 - Feels Good.ogg
56. `ERWORLD`	Season 3 (days 182-300_ March 2021-July 2021)/209d Samsung Galaxy S III - Underwater world.ogg
57. `DYNAMIC`	Season 3 (days 182-300_ March 2021-July 2021)/224n Motorola RAZR V3xx - Dynamic.mp3
58. `WFLAKES`	Season 2 (days 92-181_ December 2020-March 2021)/113d Windows Mobile Ring Tone Pack 3 - Snow Flakes.wma
59. `TJINGLE`	Season 1 (days 1-91_ September 2020-December 2020)/62 T-Jingle (2006).mp3
60. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
61. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
62. `60GREEN`	Season 4 (days 301-present_ July 2021-present)/313d Alcatel OT-660 - Green.mp3
63. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
64. `YS8AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/198n Samsung Galaxy S8 - Aqua.ogg
65. `LSTREET`	Season 4 (days 301-present_ July 2021-present)/332d Sony Ericsson P1i - Hill street.mp3
66. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
67. `GHTLIFE`	Season 3 (days 182-300_ March 2021-July 2021)/220n Motorola E365 - Night Life.wav
68. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
69. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
70. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
71. `EDITION`	Season 3 (days 182-300_ March 2021-July 2021)/195n Android 1.5 Cupcake - Champagne Edition.wav
72. `ALIENTE`	Season 1 (days 1-91_ September 2020-December 2020)/75 Windows Mobile (downloadable) - Ballroom Caliente.wma
73. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
74. `NCOFIRE`	Season 2 (days 92-181_ December 2020-March 2021)/158d Windows Mobile Ring Tone Pack 2 - Flamenco Fire.wma
75. `TERNOON`	Season 2 (days 92-181_ December 2020-March 2021)/135n LG KG320 - Lazy Afternoon.flac
76. `ERUNWAY`	Season 4 (days 301-present_ July 2021-present)/389n LG dLite - Runway.mp3
77. `NEALARM`	Season 4 (days 301-present_ July 2021-present)/328n iPhone - Alarm.m4a
78. `LLBLEND`	Season 2 (days 92-181_ December 2020-March 2021)/152d Android 1.0 - Curve Ball Blend.ogg
79. `1ORANGE`	Season 1 (days 1-91_ September 2020-December 2020)/67 Xiaomi Mi A1 - Orange.ogg
80. `0DAMMAR`	Season 4 (days 301-present_ July 2021-present)/327n HTC 10 - Dammar.wav
81. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
82. `55BRILL`	Season 3 (days 182-300_ March 2021-July 2021)/257d Nokia E55 - Brill.aac
83. `CLAPPER`	Season 4 (days 301-present_ July 2021-present)/346d Nokia N78 - Clapper.mp4
84. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3
85. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
86. `POPXRAY`	Season 4 (days 301-present_ July 2021-present)/382n Alcatel M_POP - X-Ray.mp3
87. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
88. `0CIRCUS`	Season 3 (days 182-300_ March 2021-July 2021)/188d Motorola RIZR Z10 - Circus.mp3
89. `380GLOW`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow.aac
90. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
91. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
92. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
93. `97BEATS`	Season 1 (days 1-91_ September 2020-December 2020)/33 Motorola V197 - Beats.mp3
94. `DEMO044`	Season 3 (days 182-300_ March 2021-July 2021)/244d Yamaha SMAF (downloadable) - MA Sound Demo044.wav
95. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
96. `PDRAGON`	Season 2 (days 92-181_ December 2020-March 2021)/165d Nokia 7710 - Snapdragon.aac
97. `9FRIDAY`	Season 1 (days 1-91_ September 2020-December 2020)/39 Samsung Galaxy S9 - Friday.ogg
98. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
99. `AYDREAM`	Season 3 (days 182-300_ March 2021-July 2021)/262d Windows Mobile Ring Tone Pack 5 - Day Dream.wma
100. `INGTO~1`	Season 3 (days 182-300_ March 2021-July 2021)/266n Nokia Audio themes - Jazz ringtone.aac
101. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
102. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
103. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
104. `DOSCOPE`	Season 2 (days 92-181_ December 2020-March 2021)/92n Motorola E365 - Kaleidoscope.flac
105. `RAINBOW`	Season 3 (days 182-300_ March 2021-July 2021)/192n Nokia 113 - Triple rainbow.aac
106. `TICTONE`	Season 1 (days 1-91_ September 2020-December 2020)/28 Samsung Galaxy S5 - Mystic Tone.ogg
107. `ARNIVAL`	Season 3 (days 182-300_ March 2021-July 2021)/284d Samsung Galaxy S20 - Carnival.ogg
108. `DEMO041`	Season 3 (days 182-300_ March 2021-July 2021)/190n Yamaha SMAF (downloadable) - MA Sound Demo041.wav
109. `MINATED`	Season 1 (days 1-91_ September 2020-December 2020)/22 Android - Terminated.ogg
110. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
111. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
112. `DAWNING`	Season 1 (days 1-91_ September 2020-December 2020)/37 Nokia 8800 Sirocco Edition - Dawning.aac
113. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
114. `ENALINE`	Season 3 (days 182-300_ March 2021-July 2021)/195d Motorola KRZR K1 - Adrenaline.mp3
115. `ILLASKY`	Season 1 (days 1-91_ September 2020-December 2020)/54 LG KG240 - Vanilla Sky.flac
116. `INGWALK`	Season 4 (days 301-present_ July 2021-present)/302n LG Lotus - Evening Walk.mp3
117. `FUNTIME`	Season 4 (days 301-present_ July 2021-present)/339d LG CE110 - Fun-Time.mp3
118. `0SUNSET`	Season 2 (days 92-181_ December 2020-March 2021)/160n Motorola A1000 - Sunset.mp3
119. `TELLITE`	Season 2 (days 92-181_ December 2020-March 2021)/94n Samsung Galaxy S10 - Satellite.ogg
120. `BEANICE`	Season 2 (days 92-181_ December 2020-March 2021)/101d Android 1.0 - Caribbean Ice.ogg
121. `SEASIDE`	Season 1 (days 1-91_ September 2020-December 2020)/26 iOS 7 - By The Seaside.m4r
122. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
123. `INLOOPS`	Season 1 (days 1-91_ September 2020-December 2020)/74 Motorola RAZR V3c - Latin Loops.mp3
124. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
125. `CECRAFT`	Season 3 (days 182-300_ March 2021-July 2021)/221n Samsung Galaxy S7 - Spacecraft.ogg
126. `LOTHEME`	Season 3 (days 182-300_ March 2021-July 2021)/223n Windows Mobile (downloadable) - Halo Theme.mp3
127. `ASTPACE`	Season 2 (days 92-181_ December 2020-March 2021)/155d Motorola Q9m - Fast Pace.mp3
128. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
129. `ANORIFF`	Season 3 (days 182-300_ March 2021-July 2021)/238d iPhone - Piano Riff.m4a
130. `5MINGLE`	Season 2 (days 92-181_ December 2020-March 2021)/125d Nokia N85 - Mingle.aac
131. `ECIRCUS`	Season 4 (days 301-present_ July 2021-present)/362n Motorola A3100 - Space Circus.mp3
132. `REVERIE`	Season 2 (days 92-181_ December 2020-March 2021)/145d Samsung Ego - Reverie.mp3
133. `NGTONE7`	Season 1 (days 1-91_ September 2020-December 2020)/23 Windows 7 Beta - Ringtone 7.wma
134. `INAMASK`	Season 4 (days 301-present_ July 2021-present)/367d Motorola V191 (China) - Mask.mp3
135. `DEMO055`	Season 3 (days 182-300_ March 2021-July 2021)/282d Yamaha SMAF (downloadable) - MA Sound Demo055.wav
136. `ENDANCE`	Season 4 (days 301-present_ July 2021-present)/317n HP iPAQ Glisten - Dance.mp3
137. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
138. `WFLAK~1`	Season 2 (days 92-181_ December 2020-March 2021)/115d Nokia 6681 - Snowflakes.flac
139. `INGTO~2`	Season 4 (days 301-present_ July 2021-present)/319d Nokia Audio themes - Golf ringtone.aac
140. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
141. `RALPARK`	Season 3 (days 182-300_ March 2021-July 2021)/224d LG Sentio - Central Park.mp3
142. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
143. `GTRICKS`	Season 3 (days 182-300_ March 2021-July 2021)/267n Nokia 6101b - Playing tricks.mp3
144. `IGHTIDE`	Season 4 (days 301-present_ July 2021-present)/373d Samsung Galaxy S III - High tide.ogg
145. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
146. `ASTWARD`	Season 4 (days 301-present_ July 2021-present)/343n Alcatel OT-708X - Eastward.mp3
147. `EAKDOWN`	Season 4 (days 301-present_ July 2021-present)/377n Motorola RAZR V3i - Breakdown.mp3
148. `ACKDIVE`	Season 3 (days 182-300_ March 2021-July 2021)/230n Samsung Jack - Dive.mp3
149. `ONDDAWN`	Season 4 (days 301-present_ July 2021-present)/322d HTC Touch Diamond - Dawn.wav
150. `Q9HGLOW`	Season 4 (days 301-present_ July 2021-present)/357n Motorola Q9h - Glow.mp3
151. `KIDRIVE`	Season 4 (days 301-present_ July 2021-present)/337n HTC One X - Alki Drive.mp3
152. `YFUTURE`	Season 3 (days 182-300_ March 2021-July 2021)/260n Sony Ericsson Vivaz - X-ray future.m4a
153. `EDUBLIN`	Season 4 (days 301-present_ July 2021-present)/363d T-Mobile Sidekick Slide - Dublin.mp3
154. `OADTRIP`	Season 2 (days 92-181_ December 2020-March 2021)/146d Android 1.5 Cupcake - Road Trip.wav
155. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
156. `HOLIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/116n Samsung Galaxy S8 - Holiday.ogg
157. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
158. `FORWARD`	Season 4 (days 301-present_ July 2021-present)/303n T-Mobile Shadow - Moving Forward.wma
159. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
160. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
161. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
162. `NFORMER`	Season 3 (days 182-300_ March 2021-July 2021)/247n HTC One M9 - Informer.mp3
163. `LLYWOOD`	Season 3 (days 182-300_ March 2021-July 2021)/208d Android 1.5 Cupcake - Bollywood.wav
164. `LATIN~1`	Season 3 (days 182-300_ March 2021-July 2021)/272d Motorola Q9m - Platinum.mp3
165. `INGTO~3`	Season 3 (days 182-300_ March 2021-July 2021)/251d Nokia Audio themes - Car videoringtone.aac
166. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
167. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
168. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
169. `ACKHOLE`	Season 4 (days 301-present_ July 2021-present)/387d Nokia Tones - Black Hole.wma
170. `Q9HNOVA`	Season 4 (days 301-present_ July 2021-present)/301n Motorola Q9h - Nova.mp3
171. `OOKBACK`	Season 3 (days 182-300_ March 2021-July 2021)/185n Samsung B5702 DuoS - Look Back.mp3
172. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
173. `REDIBLE`	Season 3 (days 182-300_ March 2021-July 2021)/280d Nokia 6270 - Elecredible.aac
174. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
175. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
176. `RASONIC`	Season 1 (days 1-91_ September 2020-December 2020)/83 Motorola E398 - Ultra Sonic.mp3
177. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
178. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.3gp
179. `90BIKER`	Season 2 (days 92-181_ December 2020-March 2021)/119d Motorola C390 - Biker.mp3
180. `00BC110`	Season 2 (days 92-181_ December 2020-March 2021)/142d Nokia 8800 - Bc110.aac
181. `LEFEVER`	Season 4 (days 301-present_ July 2021-present)/355d LG KG800 Chocolate - Jungle Fever.wav
182. `0VISION`	Season 3 (days 182-300_ March 2021-July 2021)/233d Windows Mobile 6.0 - Vision.wma
183. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
184. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
185. `IGSOLAR`	Season 3 (days 182-300_ March 2021-July 2021)/182d Motorola M702ig - Solar.3gp
186. `IDHENRY`	Season 4 (days 301-present_ July 2021-present)/344n Gionee GN305 - Horrid Henry.mp3
187. `KIATUNE`	Season 2 (days 92-181_ December 2020-March 2021)/126n Nokia 6270 - Nokia tune.aac
188. `THEBEAT`	Season 2 (days 92-181_ December 2020-March 2021)/117d Windows Mobile Ring Tone Pack 8 - Carol Of The Beat.wma
189. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
190. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
191. `TIMEPAD`	Season 4 (days 301-present_ July 2021-present)/358d Alcatel OT-660 - Time Pad.mp3
192. `BERBAND`	Season 1 (days 1-91_ September 2020-December 2020)/38 Windows Mobile (downloadable) - Rubber Band.wma
193. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
194. `NEWFLOW`	Season 1 (days 1-91_ September 2020-December 2020)/14 Motorola RAZR V3i - New Flow.mp3
195. `LIDSODA`	Season 2 (days 92-181_ December 2020-March 2021)/107d Yamaha SMAF (downloadable) - Solid Soda.flac
196. `SIONMA3`	Season 4 (days 301-present_ July 2021-present)/374d Panasonic X300 - Fusion ma-3.wav
197. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
198. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
199. `2RHODES`	Season 2 (days 92-181_ December 2020-March 2021)/95n HTC E-Club Ring Tone Pack 2 - Rhodes.MP3
200. `LSIGNAL`	Season 1 (days 1-91_ September 2020-December 2020)/36 Motorola W490 - Digital Signal.mp3
201. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
202. `5ROCKME`	Season 3 (days 182-300_ March 2021-July 2021)/235n Motorola ROKR EM35 - Rock Me.mp3
203. `ITRAILS`	Season 4 (days 301-present_ July 2021-present)/378n Nokia 5140i - Trails.wma
204. `START~1`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
205. `HARDMIX`	Season 3 (days 182-300_ March 2021-July 2021)/288d Windows Mobile Ring Tone Pack 5 - Hard Mix.wma
206. `ULTTONE`	Season 4 (days 301-present_ July 2021-present)/342n LG Venus - VZW Default Tone.mp3
207. `IGHTOWL`	Season 2 (days 92-181_ December 2020-March 2021)/175n iOS 7 - Night Owl.m4r
208. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
209. `HICBABA`	Season 4 (days 301-present_ July 2021-present)/312n Samsung Galaxy Pocket - Chic baba.ogg
210. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
211. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
212. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
213. `OPENING`	Season 2 (days 92-181_ December 2020-March 2021)/129n iOS 7 - Opening.m4r
214. `IDPLAYA`	Season 1 (days 1-91_ September 2020-December 2020)/41 Android - Playa.wav
215. `SEDRIVE`	Season 4 (days 301-present_ July 2021-present)/335d fusoxide_s Ringtones Vol. 1 - House Drive.wav
216. `DEMO021`	Season 3 (days 182-300_ March 2021-July 2021)/293d Yamaha SMAF (downloadable) - MA Sound Demo021.wav
217. `ERSURGE`	Season 2 (days 92-181_ December 2020-March 2021)/134n Motorola E1070 - Power Surge.mp3
218. `GONAVIS`	Season 3 (days 182-300_ March 2021-July 2021)/202n Android 2.3 Gingerbread - Argo Navis.ogg
219. `SAFFRON`	Season 2 (days 92-181_ December 2020-March 2021)/154d HTC Ozone - Saffron.wma
220. `GETAWAY`	Season 2 (days 92-181_ December 2020-March 2021)/161d Google Pixel 4 - Getaway.ogg
221. `EFLIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/136n Android 2.0 Eclair - Free Flight.wav
222. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
223. `UNBEAMZ`	Season 3 (days 182-300_ March 2021-July 2021)/232n Sony Ericsson Vivaz - Sunbeamz.m4a
224. `NMYSOUL`	Season 2 (days 92-181_ December 2020-March 2021)/157n Sony Ericsson T700 - In my soul.m4a
225. `SODAPOP`	Season 3 (days 182-300_ March 2021-July 2021)/248d Nokia 6300 (newer firmware) - Soda pop.aac
226. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
227. `AYNIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/152n Motorola E398 - Saturday night.mp3
228. `OVIOLET`	Season 3 (days 182-300_ March 2021-July 2021)/271d Nokia Oro - Violet.aac
229. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
230. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
231. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
232. `LDSTYLE`	Season 2 (days 92-181_ December 2020-March 2021)/96n Motorola RIZR Z3 - Wild Style.mp3
233. `INGSTAR`	Season 3 (days 182-300_ March 2021-July 2021)/185d Samsung Galaxy S10 - Shooting Star.ogg
234. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
235. `BOOKEM_`	Season 2 (days 92-181_ December 2020-March 2021)/143n HTC HD7 - Book Em_.mp3
236. `TEPPERS`	Season 3 (days 182-300_ March 2021-July 2021)/246n Panasonic G50 - Steppers.wav
237. `ICDISCO`	Season 4 (days 301-present_ July 2021-present)/390d Microsoft Office clipart - Generic Disco.wav
238. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
239. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
240. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
241. `NIVERSE`	Season 1 (days 1-91_ September 2020-December 2020)/45 Motorola SLVR L6g (Chinese version) - Little Universe.mp3
242. `TARTUP2`	Season 4 (days 301-present_ July 2021-present)/380dc KDE 3.3 - Startup 2.ogg
243. `KCHIMER`	Season 1 (days 1-91_ September 2020-December 2020)/71 Windows Mobile Ring Tone Pack - Chimer.wma
244. `00BELL6`	Season 1 (days 1-91_ September 2020-December 2020)/50 Samsung SGH-X100 - Bell 6.flac
245. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
246. `98FLUID`	Season 3 (days 182-300_ March 2021-July 2021)/260d Motorola E398 - Fluid.mp3
247. `HETRICK`	Season 3 (days 182-300_ March 2021-July 2021)/255d Nokia 6500 - The trick.aac
248. `WEETPIE`	Season 4 (days 301-present_ July 2021-present)/334d LG Viewty Smart - Sweet Pie.mp3
249. `HORIZON`	Season 2 (days 92-181_ December 2020-March 2021)/128n Samsung Galaxy S II - Over the horizon.ogg
250. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
251. `90WAVES`	Season 2 (days 92-181_ December 2020-March 2021)/135d Motorola C390 - Waves.mp3
252. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
253. `ORGANIC`	Season 3 (days 182-300_ March 2021-July 2021)/191d HTC Touch - Organic.wma
254. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
