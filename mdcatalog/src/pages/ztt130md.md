---
title: "Art of Noise - The Seduction of Claude Debussy"
date: "1999-06-28"
---

# Art Of Noise - The Seduction of Claude Debussy

## Summary

A prerecorded MD, from ZTT records. Filed under date of original release.

The track titles on the disc are all caps, I have listed them here as mixed case.

This is also referenced as part of the ZTT discography
[here](https://www.zttaat.com/release.php?item=465)

## Tracks

1. Il Pleure (At the Turn of the Century)
2. Born on a Sunday
3. Dreaming in Colour
4. On Being Blue
5. Continued in Colour
6. Rapt: In the Evening Air
7. Metaforce
8. The Holy Egoism of Genius
9. La flûte de Pan
10. Metaphor on the Floor
11. Approximate Mood Swing No:2
12. Pause
13. Out of This World (Version 138)

## Disc Notes

- All titles on the disc are uppercased and mushed into basic ASCII.
- track 7 is _excellent_.
