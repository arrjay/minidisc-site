---
title: cccbb383
date: "2022-01-15"
---

# cccbb383

## Summary

This disc was my specific exchange in the MiniDisc Mix, with 2 recipients.
Each got an identical copy. Was trying to go for something closer to deep cuts
from my library here.

## Tracks

1. [FAX Me - Yoko Kanno](https://www.youtube.com/watch?v=QqxVhzqdjBs)
2. [Swim - Madonna](https://www.youtube.com/watch?v=jSc9dCEsOuo)
3. [Running One - Tykwer, Klimek, Heil](https://www.youtube.com/watch?v=4k7D8dhBrXU)
4. [Back And Forth - Doctor Steel](https://www.youtube.com/watch?v=NLTOXjb2Tvc)
5. [Bits Of Bone - The Delgados](https://www.youtube.com/watch?v=_vexFN5hpwI)
6. [Live & Direct - Sugar Ray](https://www.youtube.com/watch?v=RNVjZZ0fkzw)
7. [Flute Loop - Beastie Boys](https://www.youtube.com/watch?v=BpBIuhO5QQs)
8. [Out Of Sight - Smash Mouth](https://www.youtube.com/watch?v=fG5fKpU951U)
9. [Fashionably Uninvited - Mellowdrone](https://www.youtube.com/watch?v=SeI3DXu7hHo)
10. [Don't Hurt Yourself - Marillion](https://www.youtube.com/watch?v=AXMOMfQglho)
11. [Rebel Rebel - David Bowie](https://youtu.be/wKx_PH3M0ho?t=260)
12. [The Reptiles And I - Shriekback](https://www.youtube.com/watch?v=7q2HAefWrBA)
13. [Wave Motion Gun - Marcy Playground](https://www.youtube.com/watch?v=WIIDvMxutSE)
14. [Beat It \[Moby's Sub Mix\] - Michael Jackson](https://www.youtube.com/watch?v=EfhVxVefNhk)
15. [Digital Love \[Boris Dlugosh Remix\] - Daft Punk](https://www.youtube.com/watch?v=ZoysjE6kSRY)
16. [Crystalised - The Xx](https://www.youtube.com/watch?v=Pib8eYDSFEI)
17. [Makka na Bara to Gin Tonic - Sakai & Mizumori](https://www.youtube.com/watch?v=U2xVTE-2eik)
18. [Stay Entertained - Stellastarr\*](https://www.youtube.com/watch?v=IrAEE5w7SJY)

## Mixing/Sourcing Notes

- I have some questionable things in my media library.
- I'll admit getting anything out of me in January is a terrible idea at the
best of times, a lot of family birthdays in here.
- I don't remember how the discs are titled! I built this from the m3u file
I had exported.
- Back and Forth has [a music video](https://www.youtube.com/watch?v=yCKkcex6_Hg).
It's a thing.
- Apparently, so does [Don't Hurt Yourself](https://www.youtube.com/watch?v=7KaZSj262Tc)
- David Bowie's Story Tellers is awesome and everyone should listen to that.
- I swear the "Beat It" remix is something out of Mario Kart.
- But now I really just want to play Katamari Damacy.
