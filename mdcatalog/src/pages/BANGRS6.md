---
title: BANGRS6
date: "2021-12-16"
---

# BANGRS6

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `0GLOWHQ`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow (HQ).wav
3. `LSIGNAL`	Season 1 (days 1-91_ September 2020-December 2020)/36 Motorola W490 - Digital Signal.mp3
4. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
5. `CATWALK`	Season 3 (days 182-300_ March 2021-July 2021)/184n Nokia 5300 - Catwalk.aac
6. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
7. `NEWFLOW`	Season 1 (days 1-91_ September 2020-December 2020)/14 Motorola RAZR V3i - New Flow.mp3
8. `OFORION`	Season 2 (days 92-181_ December 2020-March 2021)/169d BenQ-Siemens E71 - Bells of Orion.wav
9. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
10. `2PEPPER`	Season 3 (days 182-300_ March 2021-July 2021)/205n Nokia N72 - Pepper.aac
11. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
12. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
13. `95GLINT`	Season 3 (days 182-300_ March 2021-July 2021)/216n Nokia N95 - Glint.aac
14. `KIDRIVE`	Season 4 (days 301-present_ July 2021-present)/337n HTC One X - Alki Drive.mp3
15. `ERUNWAY`	Season 4 (days 301-present_ July 2021-present)/389n LG dLite - Runway.mp3
16. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380dh Ubuntu 6.10 - Startup.wav
17. `RADIATE`	Season 2 (days 92-181_ December 2020-March 2021)/149n iOS 7 - Radiate.m4r
18. `LATINUM`	Season 3 (days 182-300_ March 2021-July 2021)/272d Motorola Q9m - Platinum.mp3
19. `ACKDIVE`	Season 3 (days 182-300_ March 2021-July 2021)/230n Samsung Jack - Dive.mp3
20. `TJINGLE`	Season 1 (days 1-91_ September 2020-December 2020)/62 T-Jingle (2006).mp3
21. `START~1`	Season 4 (days 301-present_ July 2021-present)/380de Mandriva Linux 2006 - Startup.wav
22. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
23. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
24. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
25. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
26. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
27. `ASTWARD`	Season 4 (days 301-present_ July 2021-present)/343n Alcatel OT-708X - Eastward.mp3
28. `TELLITE`	Season 2 (days 92-181_ December 2020-March 2021)/94n Samsung Galaxy S10 - Satellite.ogg
29. `BAGITUP`	Season 1 (days 1-91_ September 2020-December 2020)/11 BenQ-Siemens S68 - Bag it up.wav
30. `BOOKEM_`	Season 2 (days 92-181_ December 2020-March 2021)/143n HTC HD7 - Book Em_.mp3
31. `ULLMOON`	Season 1 (days 1-91_ September 2020-December 2020)/61 Windows Mobile Ring Tone Pack 6 - Full Moon.wma
32. `EPUZZLE`	Season 2 (days 92-181_ December 2020-March 2021)/159d Yamaha SMAF (downloadable) - Generate Puzzle.flac
33. `ERIMENT`	Season 4 (days 301-present_ July 2021-present)/347n Nokia N9 - Noise experiment.mp3
34. `KCHIMER`	Season 1 (days 1-91_ September 2020-December 2020)/71 Windows Mobile Ring Tone Pack - Chimer.wma
35. `NE7PURE`	Season 1 (days 1-91_ September 2020-December 2020)/08 Windows Phone 7 - Pure.wma
36. `FORWARD`	Season 4 (days 301-present_ July 2021-present)/303n T-Mobile Shadow - Moving Forward.wma
37. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
38. `GHTCLUB`	Season 3 (days 182-300_ March 2021-July 2021)/262n Motorola ROKR Z6 - Nightclub.mp3
39. `AMBIENT`	Season 3 (days 182-300_ March 2021-July 2021)/292n Motorola RAZR V3i - Ambient.mp3
40. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
41. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
42. `INGSTAR`	Season 3 (days 182-300_ March 2021-July 2021)/185d Samsung Galaxy S10 - Shooting Star.ogg
43. `VOLTAGE`	Season 1 (days 1-91_ September 2020-December 2020)/59 BenQ-Siemens S68 - High Voltage.wav
44. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
45. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
46. `LAQRIFF`	Season 4 (days 301-present_ July 2021-present)/342d Motorola Q - Riff.mp3
47. `CLAPPER`	Season 4 (days 301-present_ July 2021-present)/346d Nokia N78 - Clapper.mp4
48. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
49. `HTSWING`	Season 4 (days 301-present_ July 2021-present)/365n Samsung Galaxy S9 - Midnight Swing.ogg
50. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
51. `ECIRCUS`	Season 4 (days 301-present_ July 2021-present)/362n Motorola A3100 - Space Circus.mp3
52. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
53. `SEDRIVE`	Season 4 (days 301-present_ July 2021-present)/335d fusoxide_s Ringtones Vol. 1 - House Drive.wav
54. `HARDMIX`	Season 3 (days 182-300_ March 2021-July 2021)/288d Windows Mobile Ring Tone Pack 5 - Hard Mix.wma
55. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
56. `NGTONE7`	Season 1 (days 1-91_ September 2020-December 2020)/23 Windows 7 Beta - Ringtone 7.wma
57. `T_TTONE`	Season 2 (days 92-181_ December 2020-March 2021)/130n AT_T - AT_T Tone.wav
58. `GHTLIFE`	Season 3 (days 182-300_ March 2021-July 2021)/220n Motorola E365 - Night Life.wav
59. `ITCOMES`	Season 3 (days 182-300_ March 2021-July 2021)/268d Samsung SGH-i400 - Here it comes.mp3
60. `IGSOLAR`	Season 3 (days 182-300_ March 2021-July 2021)/182d Motorola M702ig - Solar.3gp
61. `WEETPIE`	Season 4 (days 301-present_ July 2021-present)/334d LG Viewty Smart - Sweet Pie.mp3
62. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
63. `OLAQDOT`	Season 4 (days 301-present_ July 2021-present)/385n Motorola Q - Dot.mp3
64. `GTONE17`	Season 3 (days 182-300_ March 2021-July 2021)/277n LG enV2 - Ringtone 17.wav
65. `DEMO041`	Season 3 (days 182-300_ March 2021-July 2021)/190n Yamaha SMAF (downloadable) - MA Sound Demo041.wav
66. `6LIQUID`	Season 3 (days 182-300_ March 2021-July 2021)/183n Motorola SLVR L6 - Liquid.mp3
67. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
68. `ENDANCE`	Season 4 (days 301-present_ July 2021-present)/317n HP iPAQ Glisten - Dance.mp3
69. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
70. `ASTPACE`	Season 2 (days 92-181_ December 2020-March 2021)/155d Motorola Q9m - Fast Pace.mp3
71. `EADNEON`	Season 1 (days 1-91_ September 2020-December 2020)/79 Android 2.3 Gingerbread - Neon.wav
72. `JUPITER`	Season 3 (days 182-300_ March 2021-July 2021)/270d Alcatel OT-660 - Jupiter.mp3
73. `START~2`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
74. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
75. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
76. `55BRILL`	Season 3 (days 182-300_ March 2021-July 2021)/257d Nokia E55 - Brill.aac
77. `NFORMER`	Season 3 (days 182-300_ March 2021-July 2021)/247n HTC One M9 - Informer.mp3
78. `CEPTRUM`	Season 3 (days 182-300_ March 2021-July 2021)/265n Android 2.3 Gingerbread - Sceptrum.ogg
79. `AYNIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/152n Motorola E398 - Saturday night.mp3
80. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
81. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
82. `LLATION`	Season 3 (days 182-300_ March 2021-July 2021)/240n Motorola ROKR EM35 - Oscillation.mp3
83. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
84. `8IRISED`	Season 3 (days 182-300_ March 2021-July 2021)/259d Alcatel OT-808 - Irised.mp3
85. `OULMATE`	Season 4 (days 301-present_ July 2021-present)/351n Samsung SGH-D980 DUOS - Melody5 (Soul mate).wav
86. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
87. `HANGING`	Season 4 (days 301-present_ July 2021-present)/315d Motorola A3100 - Hanging.mp3
88. `THETONE`	Season 4 (days 301-present_ July 2021-present)/306n Android 0.9 - Romancing The Tone.ogg
89. `ROHOUSE`	Season 3 (days 182-300_ March 2021-July 2021)/296d Samsung PC Studio - Euro House.wav
90. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
91. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
92. `0DAMMAR`	Season 4 (days 301-present_ July 2021-present)/327n HTC 10 - Dammar.wav
93. `UNNYDAY`	Season 2 (days 92-181_ December 2020-March 2021)/103d Samsung C5212 - Sunny day.mp3
94. `HOLIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/116n Samsung Galaxy S8 - Holiday.ogg
95. `ORGANIC`	Season 3 (days 182-300_ March 2021-July 2021)/191d HTC Touch - Organic.wma
96. `LITHAZE`	Season 4 (days 301-present_ July 2021-present)/338d Motorola E815 - Moonlit Haze.wav
97. `2RHODES`	Season 2 (days 92-181_ December 2020-March 2021)/95n HTC E-Club Ring Tone Pack 2 - Rhodes.MP3
98. `EAKDOWN`	Season 4 (days 301-present_ July 2021-present)/377n Motorola RAZR V3i - Breakdown.mp3
99. `SCOVERY`	Season 2 (days 92-181_ December 2020-March 2021)/148n Nokia 8600 - Discovery.aac
100. `RASONIC`	Season 1 (days 1-91_ September 2020-December 2020)/83 Motorola E398 - Ultra Sonic.mp3
101. `RL7FIRE`	Season 2 (days 92-181_ December 2020-March 2021)/100d Motorola SLVR L7 - Fire.m4a
102. `ULTTONE`	Season 4 (days 301-present_ July 2021-present)/342n LG Venus - VZW Default Tone.mp3
103. `WFLAKES`	Season 2 (days 92-181_ December 2020-March 2021)/113d Windows Mobile Ring Tone Pack 3 - Snow Flakes.wma
104. `SSWIVIT`	Season 3 (days 182-300_ March 2021-July 2021)/290n Android 2.0 Eclair - Don_ Mess Wiv It.wav
105. `EFLIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/136n Android 2.0 Eclair - Free Flight.wav
106. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
107. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
108. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
109. `NODANCE`	Season 3 (days 182-300_ March 2021-July 2021)/192d Samsung SGH-P250 - Techno dance.mp3
110. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
111. `00BELL6`	Season 1 (days 1-91_ September 2020-December 2020)/50 Samsung SGH-X100 - Bell 6.flac
112. `NMYSOUL`	Season 2 (days 92-181_ December 2020-March 2021)/157n Sony Ericsson T700 - In my soul.m4a
113. `OCKTAIL`	Season 2 (days 92-181_ December 2020-March 2021)/142n Motorola RAZR V3i - Cocktail.mp3
114. `DREAMER`	Season 2 (days 92-181_ December 2020-March 2021)/162d HTC 8-Bit Sound Set - Cloud Dreamer.mp3
115. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
116. `ARNIVAL`	Season 3 (days 182-300_ March 2021-July 2021)/284d Samsung Galaxy S20 - Carnival.ogg
117. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
118. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
119. `DEMO044`	Season 3 (days 182-300_ March 2021-July 2021)/244d Yamaha SMAF (downloadable) - MA Sound Demo044.wav
120. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
121. `0AMINOR`	Season 3 (days 182-300_ March 2021-July 2021)/197n LG S5100 - A minor.wav
122. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
123. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
124. `ICDISCO`	Season 4 (days 301-present_ July 2021-present)/390d Microsoft Office clipart - Generic Disco.wav
125. `ANDWINE`	Season 2 (days 92-181_ December 2020-March 2021)/108n LG KG280 - Cigar And Wine.flac
126. `310KICK`	Season 4 (days 301-present_ July 2021-present)/340d Nokia 3310 - Kick.wma
127. `GTONE01`	Season 1 (days 1-91_ September 2020-December 2020)/42 Windows 7 - Ringtone 01.wma
128. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
129. `REVERIE`	Season 2 (days 92-181_ December 2020-March 2021)/145d Samsung Ego - Reverie.mp3
130. `LEFEVER`	Season 4 (days 301-present_ July 2021-present)/355d LG KG800 Chocolate - Jungle Fever.wav
131. `CLASSIC`	Season 2 (days 92-181_ December 2020-March 2021)/132n Windows Phone 8 - Classic.wma
132. `PDRAGON`	Season 2 (days 92-181_ December 2020-March 2021)/165d Nokia 7710 - Snapdragon.aac
133. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
134. `000MOON`	Season 4 (days 301-present_ July 2021-present)/391d Motorola A1000 - Moon.mp3
135. `TRIPPER`	Season 4 (days 301-present_ July 2021-present)/387n HTC HD7 - Tripper.mp3
136. `LSTREET`	Season 4 (days 301-present_ July 2021-present)/332d Sony Ericsson P1i - Hill street.mp3
137. `INGWALK`	Season 4 (days 301-present_ July 2021-present)/302n LG Lotus - Evening Walk.mp3
138. `0NUANCE`	Season 2 (days 92-181_ December 2020-March 2021)/97n Nokia 6270 - Nuance.aac
139. `NVASION`	Season 4 (days 301-present_ July 2021-present)/330d Motorola Droid A855 - Droid Invasion.ogg
140. `370CLUB`	Season 2 (days 92-181_ December 2020-March 2021)/136d Nokia 7370 - Club.3gp
141. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
142. `IDHENRY`	Season 4 (days 301-present_ July 2021-present)/344n Gionee GN305 - Horrid Henry.mp3
143. `GETAWAY`	Season 2 (days 92-181_ December 2020-March 2021)/161d Google Pixel 4 - Getaway.ogg
144. `AYDREAM`	Season 3 (days 182-300_ March 2021-July 2021)/262d Windows Mobile Ring Tone Pack 5 - Day Dream.wma
145. `5RUNWAY`	Season 4 (days 301-present_ July 2021-present)/350d Nokia N85 - Runway.wma
146. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
147. `ANORIFF`	Season 3 (days 182-300_ March 2021-July 2021)/238d iPhone - Piano Riff.m4a
148. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
149. `IGHTOWL`	Season 2 (days 92-181_ December 2020-March 2021)/175n iOS 7 - Night Owl.m4r
150. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
151. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
152. `1ORANGE`	Season 1 (days 1-91_ September 2020-December 2020)/67 Xiaomi Mi A1 - Orange.ogg
153. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
154. `Q9HNOVA`	Season 4 (days 301-present_ July 2021-present)/301n Motorola Q9h - Nova.mp3
155. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
156. `NKY_ALL`	Season 4 (days 301-present_ July 2021-present)/302d Android 1.5 Cupcake - Funk Y_all.wav
157. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
158. `THEDARK`	Season 4 (days 301-present_ July 2021-present)/358n Samsung SGH-Z130 - Dancing in the Dark.mp3
159. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav
160. `IHIGHER`	Season 4 (days 301-present_ July 2021-present)/348d Sony Ericsson K750i - Higher.mp3
161. `98FLUID`	Season 3 (days 182-300_ March 2021-July 2021)/260d Motorola E398 - Fluid.mp3
162. `RALPARK`	Season 3 (days 182-300_ March 2021-July 2021)/224d LG Sentio - Central Park.mp3
163. `9FRIDAY`	Season 1 (days 1-91_ September 2020-December 2020)/39 Samsung Galaxy S9 - Friday.ogg
164. `OUNGING`	Season 3 (days 182-300_ March 2021-July 2021)/276d Nokia 7370 - Lounging.wav
165. `_SIGNAL`	Season 3 (days 182-300_ March 2021-July 2021)/199n Kyocera Kona - Poppin_ Signal.ogg
166. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
167. `0CIRCUS`	Season 3 (days 182-300_ March 2021-July 2021)/188d Motorola RIZR Z10 - Circus.mp3
168. `ONDDAWN`	Season 4 (days 301-present_ July 2021-present)/322d HTC Touch Diamond - Dawn.wav
169. `CHINESE`	Season 3 (days 182-300_ March 2021-July 2021)/206n1 Nokia Tune Remake Audiodraft Sound Design Contest - NOKIA CHINESE.mp3
170. `ERDISCO`	Season 2 (days 92-181_ December 2020-March 2021)/140d Samsung Galaxy S20 - Roller Disco.ogg
171. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
172. `ERSPACE`	Season 2 (days 92-181_ December 2020-March 2021)/175d Nokia 6108 - Hyperspace.flac
173. `5ROCKME`	Season 3 (days 182-300_ March 2021-July 2021)/235n Motorola ROKR EM35 - Rock Me.mp3
174. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
175. `MINATED`	Season 1 (days 1-91_ September 2020-December 2020)/22 Android - Terminated.ogg
176. `TENIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/203n Sony Ericsson T700 - Late night.m4a
177. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
178. `DAWNING`	Season 1 (days 1-91_ September 2020-December 2020)/37 Nokia 8800 Sirocco Edition - Dawning.aac
179. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
180. `60GREEN`	Season 4 (days 301-present_ July 2021-present)/313d Alcatel OT-660 - Green.mp3
181. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
182. `6WAHWAH`	Season 3 (days 182-300_ March 2021-July 2021)/287n Nokia 2626 - Wah wah.mp3
183. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
184. `IDPLAYA`	Season 1 (days 1-91_ September 2020-December 2020)/41 Android - Playa.wav
185. `RE2KICK`	Season 4 (days 301-present_ July 2021-present)/359n Motorola ROKR E2 - Kick.mp3
186. `START~3`	Season 4 (days 301-present_ July 2021-present)/380dg Ubuntu 4.10 - Startup.wav
187. `DYNAMIC`	Season 3 (days 182-300_ March 2021-July 2021)/224n Motorola RAZR V3xx - Dynamic.mp3
188. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
189. `NICBOOM`	Season 2 (days 92-181_ December 2020-March 2021)/97d Samsung Gravity SGH-T456 - Sonicboom.mp3
190. `LOTHEME`	Season 3 (days 182-300_ March 2021-July 2021)/223n Windows Mobile (downloadable) - Halo Theme.mp3
191. `CECRAFT`	Season 3 (days 182-300_ March 2021-July 2021)/221n Samsung Galaxy S7 - Spacecraft.ogg
192. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
193. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
194. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
195. `CEPARTY`	Season 1 (days 1-91_ September 2020-December 2020)/72 Samsung Galaxy S6 - Dance Party.ogg
196. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
197. `EPOLISH`	Season 3 (days 182-300_ March 2021-July 2021)/265d HTC Ozone - Polish.wma
198. `90BIKER`	Season 2 (days 92-181_ December 2020-March 2021)/119d Motorola C390 - Biker.mp3
199. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
200. `GHSCORE`	Season 2 (days 92-181_ December 2020-March 2021)/151d Windows Mobile Extended Audio Pack 1 - High Score.wma
201. `SURGENT`	Season 4 (days 301-present_ July 2021-present)/354n Sonic Logos, Links and Beds (Album 2) - It_s Urgent.mp3
202. `INGTO~1`	Season 3 (days 182-300_ March 2021-July 2021)/266n Nokia Audio themes - Jazz ringtone.aac
203. `BERBAND`	Season 1 (days 1-91_ September 2020-December 2020)/38 Windows Mobile (downloadable) - Rubber Band.wma
204. `PASSION`	Season 3 (days 182-300_ March 2021-July 2021)/198d Motorola E1000 - Passion.mp3
205. `ALIENTE`	Season 1 (days 1-91_ September 2020-December 2020)/75 Windows Mobile (downloadable) - Ballroom Caliente.wma
206. `TEPPERS`	Season 3 (days 182-300_ March 2021-July 2021)/246n Panasonic G50 - Steppers.wav
207. `SCRAPER`	Season 2 (days 92-181_ December 2020-March 2021)/98n Samsung Galaxy S8 - Skyscraper.ogg
208. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
209. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
210. `OPENING`	Season 2 (days 92-181_ December 2020-March 2021)/129n iOS 7 - Opening.m4r
211. `ECLOUDS`	Season 4 (days 301-present_ July 2021-present)/310d Sony Ericsson P1i - Into the clouds.mp3
212. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
213. `LIDSODA`	Season 2 (days 92-181_ December 2020-March 2021)/107d Yamaha SMAF (downloadable) - Solid Soda.flac
214. `SIONMA3`	Season 4 (days 301-present_ July 2021-present)/374d Panasonic X300 - Fusion ma-3.wav
215. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
216. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
217. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
218. `ELSGOOD`	Season 4 (days 301-present_ July 2021-present)/320n Samsung Galaxy S20 - Feels Good.ogg
219. `CSLEAZY`	Season 1 (days 1-91_ September 2020-December 2020)/10 Motorola E1000 - Mcsleazy.mp3
220. `GHTLI~1`	Season 4 (days 301-present_ July 2021-present)/308n GFive G9 - Night Life.ogg
221. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
222. `HRIDING`	Season 3 (days 182-300_ March 2021-July 2021)/288n Samsung Ego - Smooth riding.mp3
223. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
224. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.mp3
225. `CHHOUSE`	Season 2 (days 92-181_ December 2020-March 2021)/99d Sony Ericsson T700 - Beach house.m4a
226. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
227. `SEASIDE`	Season 1 (days 1-91_ September 2020-December 2020)/26 iOS 7 - By The Seaside.m4r
228. `ETSTYLE`	Season 4 (days 301-present_ July 2021-present)/380n Motorola W490 - Street Style.mp3
229. `TARTUP2`	Season 4 (days 301-present_ July 2021-present)/380dc KDE 3.3 - Startup 2.ogg
230. `THEBEAT`	Season 2 (days 92-181_ December 2020-March 2021)/117d Windows Mobile Ring Tone Pack 8 - Carol Of The Beat.wma
231. `ACKHOLE`	Season 4 (days 301-present_ July 2021-present)/387d Nokia Tones - Black Hole.wma
232. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
233. `POPXRAY`	Season 4 (days 301-present_ July 2021-present)/382n Alcatel M_POP - X-Ray.mp3
234. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
235. `LATIN~1`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
236. `KINGDOM`	Season 3 (days 182-300_ March 2021-July 2021)/184d Panasonic G60 - 808Kingdom.wav
237. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
238. `DEMO055`	Season 3 (days 182-300_ March 2021-July 2021)/282d Yamaha SMAF (downloadable) - MA Sound Demo055.wav
239. `INGTO~2`	Season 3 (days 182-300_ March 2021-July 2021)/256d Nokia Audio themes - Car ringtone.aac
240. `GONAVIS`	Season 3 (days 182-300_ March 2021-July 2021)/202n Android 2.3 Gingerbread - Argo Navis.ogg
241. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
242. `WFLAK~1`	Season 2 (days 92-181_ December 2020-March 2021)/115d Nokia 6681 - Snowflakes.flac
243. `5MINGLE`	Season 2 (days 92-181_ December 2020-March 2021)/125d Nokia N85 - Mingle.aac
244. `ENALINE`	Season 3 (days 182-300_ March 2021-July 2021)/195d Motorola KRZR K1 - Adrenaline.mp3
245. `ALKAWAY`	Season 3 (days 182-300_ March 2021-July 2021)/258d Nokia N85 - Walk away.aac
246. `OADTRIP`	Season 2 (days 92-181_ December 2020-March 2021)/146d Android 1.5 Cupcake - Road Trip.wav
247. `BEANICE`	Season 2 (days 92-181_ December 2020-March 2021)/101d Android 1.0 - Caribbean Ice.ogg
248. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
249. `RGOTCHA`	Season 3 (days 182-300_ March 2021-July 2021)/219d Android 2.0 Eclair - Gotcha.wav
250. `EDUBLIN`	Season 4 (days 301-present_ July 2021-present)/363d T-Mobile Sidekick Slide - Dublin.mp3
251. `RAINBOW`	Season 3 (days 182-300_ March 2021-July 2021)/192n Nokia 113 - Triple rainbow.aac
252. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
253. `INGTO~3`	Season 3 (days 182-300_ March 2021-July 2021)/251d Nokia Audio themes - Car videoringtone.aac
254. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
