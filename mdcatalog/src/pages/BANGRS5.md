---
title: BANGRS5
date: "2021-12-16"
---

# BANGRS5

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
3. `000MOON`	Season 4 (days 301-present_ July 2021-present)/391d Motorola A1000 - Moon.mp3
4. `OOKBACK`	Season 3 (days 182-300_ March 2021-July 2021)/185n Samsung B5702 DuoS - Look Back.mp3
5. `HRIDING`	Season 3 (days 182-300_ March 2021-July 2021)/288n Samsung Ego - Smooth riding.mp3
6. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3
7. `IGHTOWL`	Season 2 (days 92-181_ December 2020-March 2021)/175n iOS 7 - Night Owl.m4r
8. `TEPPERS`	Season 3 (days 182-300_ March 2021-July 2021)/246n Panasonic G50 - Steppers.wav
9. `EPOLISH`	Season 3 (days 182-300_ March 2021-July 2021)/265d HTC Ozone - Polish.wma
10. `LATINUM`	Season 3 (days 182-300_ March 2021-July 2021)/272d Motorola Q9m - Platinum.mp3
11. `0GROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/149d Motorola A1000 - Groove.mp3
12. `OFORION`	Season 2 (days 92-181_ December 2020-March 2021)/169d BenQ-Siemens E71 - Bells of Orion.wav
13. `NEWFLOW`	Season 1 (days 1-91_ September 2020-December 2020)/14 Motorola RAZR V3i - New Flow.mp3
14. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
15. `IGHTIDE`	Season 4 (days 301-present_ July 2021-present)/373d Samsung Galaxy S III - High tide.ogg
16. `SSWIVIT`	Season 3 (days 182-300_ March 2021-July 2021)/290n Android 2.0 Eclair - Don_ Mess Wiv It.wav
17. `WFLAKES`	Season 2 (days 92-181_ December 2020-March 2021)/115d Nokia 6681 - Snowflakes.flac
18. `5ROCKME`	Season 3 (days 182-300_ March 2021-July 2021)/235n Motorola ROKR EM35 - Rock Me.mp3
19. `RE2KICK`	Season 4 (days 301-present_ July 2021-present)/359n Motorola ROKR E2 - Kick.mp3
20. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
21. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
22. `NGARING`	Season 2 (days 92-181_ December 2020-March 2021)/172n Samsung Galaxy Advance - Ring A Ring.ogg
23. `INGSTAR`	Season 3 (days 182-300_ March 2021-July 2021)/185d Samsung Galaxy S10 - Shooting Star.ogg
24. `DYNAMIC`	Season 3 (days 182-300_ March 2021-July 2021)/224n Motorola RAZR V3xx - Dynamic.mp3
25. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
26. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
27. `THEBEAT`	Season 2 (days 92-181_ December 2020-March 2021)/117d Windows Mobile Ring Tone Pack 8 - Carol Of The Beat.wma
28. `NCOFIRE`	Season 2 (days 92-181_ December 2020-March 2021)/158d Windows Mobile Ring Tone Pack 2 - Flamenco Fire.wma
29. `55BRILL`	Season 3 (days 182-300_ March 2021-July 2021)/257d Nokia E55 - Brill.aac
30. `TICTONE`	Season 1 (days 1-91_ September 2020-December 2020)/28 Samsung Galaxy S5 - Mystic Tone.ogg
31. `00BC110`	Season 2 (days 92-181_ December 2020-March 2021)/142d Nokia 8800 - Bc110.aac
32. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
33. `9FRIDAY`	Season 1 (days 1-91_ September 2020-December 2020)/39 Samsung Galaxy S9 - Friday.ogg
34. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
35. `GHSCORE`	Season 2 (days 92-181_ December 2020-March 2021)/151d Windows Mobile Extended Audio Pack 1 - High Score.wma
36. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
37. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
38. `ERSURGE`	Season 2 (days 92-181_ December 2020-March 2021)/134n Motorola E1070 - Power Surge.mp3
39. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
40. `PASSION`	Season 3 (days 182-300_ March 2021-July 2021)/198d Motorola E1000 - Passion.mp3
41. `BERBAND`	Season 1 (days 1-91_ September 2020-December 2020)/38 Windows Mobile (downloadable) - Rubber Band.wma
42. `0SUNSET`	Season 2 (days 92-181_ December 2020-March 2021)/160n Motorola A1000 - Sunset.mp3
43. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
44. `ERDISCO`	Season 2 (days 92-181_ December 2020-March 2021)/140d Samsung Galaxy S20 - Roller Disco.ogg
45. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
46. `LLATION`	Season 3 (days 182-300_ March 2021-July 2021)/240n Motorola ROKR EM35 - Oscillation.mp3
47. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
48. `ERWORLD`	Season 3 (days 182-300_ March 2021-July 2021)/209d Samsung Galaxy S III - Underwater world.ogg
49. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
50. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
51. `TELLITE`	Season 2 (days 92-181_ December 2020-March 2021)/94n Samsung Galaxy S10 - Satellite.ogg
52. `KCHIMER`	Season 1 (days 1-91_ September 2020-December 2020)/71 Windows Mobile Ring Tone Pack - Chimer.wma
53. `ASTWARD`	Season 4 (days 301-present_ July 2021-present)/343n Alcatel OT-708X - Eastward.mp3
54. `JUPITER`	Season 3 (days 182-300_ March 2021-July 2021)/270d Alcatel OT-660 - Jupiter.mp3
55. `TRIPPER`	Season 4 (days 301-present_ July 2021-present)/387n HTC HD7 - Tripper.mp3
56. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
57. `ROHOUSE`	Season 3 (days 182-300_ March 2021-July 2021)/296d Samsung PC Studio - Euro House.wav
58. `GHTLIFE`	Season 4 (days 301-present_ July 2021-present)/308n GFive G9 - Night Life.ogg
59. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
60. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
61. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
62. `CHHOUSE`	Season 2 (days 92-181_ December 2020-March 2021)/99d Sony Ericsson T700 - Beach house.m4a
63. `ULLMOON`	Season 1 (days 1-91_ September 2020-December 2020)/61 Windows Mobile Ring Tone Pack 6 - Full Moon.wma
64. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
65. `NE7PURE`	Season 1 (days 1-91_ September 2020-December 2020)/08 Windows Phone 7 - Pure.wma
66. `TERNOON`	Season 2 (days 92-181_ December 2020-March 2021)/135n LG KG320 - Lazy Afternoon.flac
67. `VOLTAGE`	Season 1 (days 1-91_ September 2020-December 2020)/59 BenQ-Siemens S68 - High Voltage.wav
68. `TARTUP2`	Season 4 (days 301-present_ July 2021-present)/380dc KDE 3.3 - Startup 2.ogg
69. `EPUZZLE`	Season 2 (days 92-181_ December 2020-March 2021)/159d Yamaha SMAF (downloadable) - Generate Puzzle.flac
70. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
71. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
72. `ULTTONE`	Season 4 (days 301-present_ July 2021-present)/342n LG Venus - VZW Default Tone.mp3
73. `GHTCLUB`	Season 3 (days 182-300_ March 2021-July 2021)/262n Motorola ROKR Z6 - Nightclub.mp3
74. `OVIOLET`	Season 3 (days 182-300_ March 2021-July 2021)/271d Nokia Oro - Violet.aac
75. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
76. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
77. `00BELL6`	Season 1 (days 1-91_ September 2020-December 2020)/50 Samsung SGH-X100 - Bell 6.flac
78. `WFLAK~1`	Season 2 (days 92-181_ December 2020-March 2021)/113d Windows Mobile Ring Tone Pack 3 - Snow Flakes.wma
79. `ANORIFF`	Season 3 (days 182-300_ March 2021-July 2021)/238d iPhone - Piano Riff.m4a
80. `OADTRIP`	Season 2 (days 92-181_ December 2020-March 2021)/146d Android 1.5 Cupcake - Road Trip.wav
81. `DEMO044`	Season 3 (days 182-300_ March 2021-July 2021)/244d Yamaha SMAF (downloadable) - MA Sound Demo044.wav
82. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
83. `ITRAILS`	Season 4 (days 301-present_ July 2021-present)/378n Nokia 5140i - Trails.wma
84. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
85. `KIDRIVE`	Season 4 (days 301-present_ July 2021-present)/337n HTC One X - Alki Drive.mp3
86. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
87. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
88. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
89. `HICBABA`	Season 4 (days 301-present_ July 2021-present)/312n Samsung Galaxy Pocket - Chic baba.ogg
90. `RL7FIRE`	Season 2 (days 92-181_ December 2020-March 2021)/100d Motorola SLVR L7 - Fire.m4a
91. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
92. `Q9HNOVA`	Season 4 (days 301-present_ July 2021-present)/301n Motorola Q9h - Nova.mp3
93. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
94. `AMBIENT`	Season 3 (days 182-300_ March 2021-July 2021)/292n Motorola RAZR V3i - Ambient.mp3
95. `TARTUP3`	Season 4 (days 301-present_ July 2021-present)/380dd KDE 3.3 - Startup 3.ogg
96. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
97. `KINGDOM`	Season 3 (days 182-300_ March 2021-July 2021)/184d Panasonic G60 - 808Kingdom.wav
98. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
99. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
100. `EAKDOWN`	Season 4 (days 301-present_ July 2021-present)/377n Motorola RAZR V3i - Breakdown.mp3
101. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
102. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
103. `ILLASKY`	Season 1 (days 1-91_ September 2020-December 2020)/54 LG KG240 - Vanilla Sky.flac
104. `ACKHOLE`	Season 4 (days 301-present_ July 2021-present)/387d Nokia Tones - Black Hole.wma
105. `QBOOGIE`	Season 4 (days 301-present_ July 2021-present)/383n Windows Mobile (downloadable) - BBQ Boogie.wma
106. `0AMINOR`	Season 3 (days 182-300_ March 2021-July 2021)/197n LG S5100 - A minor.wav
107. `GHTLI~1`	Season 3 (days 182-300_ March 2021-July 2021)/220n Motorola E365 - Night Life.wav
108. `Q9HGLOW`	Season 4 (days 301-present_ July 2021-present)/357n Motorola Q9h - Glow.mp3
109. `RASONIC`	Season 1 (days 1-91_ September 2020-December 2020)/83 Motorola E398 - Ultra Sonic.mp3
110. `SAFFRON`	Season 2 (days 92-181_ December 2020-March 2021)/154d HTC Ozone - Saffron.wma
111. `OCOLATE`	Season 2 (days 92-181_ December 2020-March 2021)/115n Windows Mobile Ring Tone Pack 3 - Hot Chocolate.wma
112. `LEFEVER`	Season 4 (days 301-present_ July 2021-present)/355d LG KG800 Chocolate - Jungle Fever.wav
113. `ROOVING`	Season 3 (days 182-300_ March 2021-July 2021)/227d Motorola M702ig - Grooving.3gp
114. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
115. `SEDRIVE`	Season 4 (days 301-present_ July 2021-present)/335d fusoxide_s Ringtones Vol. 1 - House Drive.wav
116. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
117. `LITHAZE`	Season 4 (days 301-present_ July 2021-present)/338d Motorola E815 - Moonlit Haze.wav
118. `GTRICKS`	Season 3 (days 182-300_ March 2021-July 2021)/267n Nokia 6101b - Playing tricks.mp3
119. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
120. `GONAVIS`	Season 3 (days 182-300_ March 2021-July 2021)/202n Android 2.3 Gingerbread - Argo Navis.ogg
121. `OVATION`	Season 2 (days 92-181_ December 2020-March 2021)/127d HTC E-Club Ring Tone Pack 1 - HTC Innovation.MP3
122. `HARDMIX`	Season 3 (days 182-300_ March 2021-July 2021)/288d Windows Mobile Ring Tone Pack 5 - Hard Mix.wma
123. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
124. `TENIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/203n Sony Ericsson T700 - Late night.m4a
125. `ETSTYLE`	Season 4 (days 301-present_ July 2021-present)/380n Motorola W490 - Street Style.mp3
126. `NGTONE7`	Season 1 (days 1-91_ September 2020-December 2020)/23 Windows 7 Beta - Ringtone 7.wma
127. `ASTPACE`	Season 2 (days 92-181_ December 2020-March 2021)/155d Motorola Q9m - Fast Pace.mp3
128. `97BEATS`	Season 1 (days 1-91_ September 2020-December 2020)/33 Motorola V197 - Beats.mp3
129. `KIATUNE`	Season 2 (days 92-181_ December 2020-March 2021)/126n Nokia 6270 - Nokia tune.aac
130. `ALKAWAY`	Season 3 (days 182-300_ March 2021-July 2021)/258d Nokia N85 - Walk away.aac
131. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
132. `REDIBLE`	Season 3 (days 182-300_ March 2021-July 2021)/280d Nokia 6270 - Elecredible.aac
133. `NEALARM`	Season 4 (days 301-present_ July 2021-present)/328n iPhone - Alarm.m4a
134. `2PEPPER`	Season 3 (days 182-300_ March 2021-July 2021)/205n Nokia N72 - Pepper.aac
135. `RGOTCHA`	Season 3 (days 182-300_ March 2021-July 2021)/219d Android 2.0 Eclair - Gotcha.wav
136. `BOOKEM_`	Season 2 (days 92-181_ December 2020-March 2021)/143n HTC HD7 - Book Em_.mp3
137. `0DAMMAR`	Season 4 (days 301-present_ July 2021-present)/327n HTC 10 - Dammar.wav
138. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
139. `1ORANGE`	Season 1 (days 1-91_ September 2020-December 2020)/67 Xiaomi Mi A1 - Orange.ogg
140. `INGTO~1`	Season 4 (days 301-present_ July 2021-present)/319d Nokia Audio themes - Golf ringtone.aac
141. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
142. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
143. `8IRISED`	Season 3 (days 182-300_ March 2021-July 2021)/259d Alcatel OT-808 - Irised.mp3
144. `OUNGING`	Season 3 (days 182-300_ March 2021-July 2021)/276d Nokia 7370 - Lounging.wav
145. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
146. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
147. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
148. `OULMATE`	Season 4 (days 301-present_ July 2021-present)/351n Samsung SGH-D980 DUOS - Melody5 (Soul mate).wav
149. `CATWALK`	Season 3 (days 182-300_ March 2021-July 2021)/184n Nokia 5300 - Catwalk.aac
150. `CLASSIC`	Season 2 (days 92-181_ December 2020-March 2021)/132n Windows Phone 8 - Classic.wma
151. `ECLOUDS`	Season 4 (days 301-present_ July 2021-present)/310d Sony Ericsson P1i - Into the clouds.mp3
152. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
153. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
154. `90WAVES`	Season 2 (days 92-181_ December 2020-March 2021)/135d Motorola C390 - Waves.mp3
155. `SODAPOP`	Season 3 (days 182-300_ March 2021-July 2021)/248d Nokia 6300 (newer firmware) - Soda pop.aac
156. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
157. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
158. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
159. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
160. `HETRICK`	Season 3 (days 182-300_ March 2021-July 2021)/255d Nokia 6500 - The trick.aac
161. `LOTHEME`	Season 3 (days 182-300_ March 2021-July 2021)/223n Windows Mobile (downloadable) - Halo Theme.mp3
162. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
163. `OPENING`	Season 2 (days 92-181_ December 2020-March 2021)/129n iOS 7 - Opening.m4r
164. `DEMO041`	Season 3 (days 182-300_ March 2021-July 2021)/190n Yamaha SMAF (downloadable) - MA Sound Demo041.wav
165. `K3SHINE`	Season 2 (days 92-181_ December 2020-March 2021)/164d Motorola KRZR K3 - Shine.mp3
166. `FUNTIME`	Season 4 (days 301-present_ July 2021-present)/339d LG CE110 - Fun-Time.mp3
167. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
168. `90BIKER`	Season 2 (days 92-181_ December 2020-March 2021)/119d Motorola C390 - Biker.mp3
169. `LDSTYLE`	Season 2 (days 92-181_ December 2020-March 2021)/96n Motorola RIZR Z3 - Wild Style.mp3
170. `LATIN~1`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
171. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
172. `6LIQUID`	Season 3 (days 182-300_ March 2021-July 2021)/183n Motorola SLVR L6 - Liquid.mp3
173. `NODANCE`	Season 3 (days 182-300_ March 2021-July 2021)/192d Samsung SGH-P250 - Techno dance.mp3
174. `0NUANCE`	Season 2 (days 92-181_ December 2020-March 2021)/97n Nokia 6270 - Nuance.aac
175. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
176. `THETONE`	Season 4 (days 301-present_ July 2021-present)/306n Android 0.9 - Romancing The Tone.ogg
177. `5MINGLE`	Season 2 (days 92-181_ December 2020-March 2021)/125d Nokia N85 - Mingle.aac
178. `START~1`	Season 4 (days 301-present_ July 2021-present)/380dg Ubuntu 4.10 - Startup.wav
179. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
180. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
181. `NFORMER`	Season 3 (days 182-300_ March 2021-July 2021)/247n HTC One M9 - Informer.mp3
182. `IHIGHER`	Season 4 (days 301-present_ July 2021-present)/348d Sony Ericsson K750i - Higher.mp3
183. `TJINGLE`	Season 1 (days 1-91_ September 2020-December 2020)/62 T-Jingle (2006).mp3
184. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
185. `GTONE01`	Season 1 (days 1-91_ September 2020-December 2020)/42 Windows 7 - Ringtone 01.wma
186. `DEMO021`	Season 3 (days 182-300_ March 2021-July 2021)/293d Yamaha SMAF (downloadable) - MA Sound Demo021.wav
187. `AYNIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/152n Motorola E398 - Saturday night.mp3
188. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
189. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
190. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
191. `IDHENRY`	Season 4 (days 301-present_ July 2021-present)/344n Gionee GN305 - Horrid Henry.mp3
192. `UNNYDAY`	Season 2 (days 92-181_ December 2020-March 2021)/103d Samsung C5212 - Sunny day.mp3
193. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
194. `RADIATE`	Season 2 (days 92-181_ December 2020-March 2021)/149n iOS 7 - Radiate.m4r
195. `60GREEN`	Season 4 (days 301-present_ July 2021-present)/313d Alcatel OT-660 - Green.mp3
196. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
197. `DEMO055`	Season 3 (days 182-300_ March 2021-July 2021)/282d Yamaha SMAF (downloadable) - MA Sound Demo055.wav
198. `THEDARK`	Season 4 (days 301-present_ July 2021-present)/358n Samsung SGH-Z130 - Dancing in the Dark.mp3
199. `OLAQDOT`	Season 4 (days 301-present_ July 2021-present)/385n Motorola Q - Dot.mp3
200. `SIONMA3`	Season 4 (days 301-present_ July 2021-present)/374d Panasonic X300 - Fusion ma-3.wav
201. `ALIENTE`	Season 1 (days 1-91_ September 2020-December 2020)/75 Windows Mobile (downloadable) - Ballroom Caliente.wma
202. `GFUSION`	Season 3 (days 182-300_ March 2021-July 2021)/209n Motorola M702ig - Fusion.3gp
203. `LAQRIFF`	Season 4 (days 301-present_ July 2021-present)/342d Motorola Q - Riff.mp3
204. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
205. `HORIZON`	Season 2 (days 92-181_ December 2020-March 2021)/128n Samsung Galaxy S II - Over the horizon.ogg
206. `DOSCOPE`	Season 2 (days 92-181_ December 2020-March 2021)/92n Motorola E365 - Kaleidoscope.flac
207. `CEPARTY`	Season 1 (days 1-91_ September 2020-December 2020)/72 Samsung Galaxy S6 - Dance Party.ogg
208. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
209. `0VISION`	Season 3 (days 182-300_ March 2021-July 2021)/233d Windows Mobile 6.0 - Vision.wma
210. `EDITION`	Season 3 (days 182-300_ March 2021-July 2021)/195n Android 1.5 Cupcake - Champagne Edition.wav
211. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
212. `SCRAPER`	Season 2 (days 92-181_ December 2020-March 2021)/98n Samsung Galaxy S8 - Skyscraper.ogg
213. `LLREMIX`	Season 1 (days 1-91_ September 2020-December 2020)/63 Discord - Incoming Call (Remix).mp3
214. `INGTO~2`	Season 3 (days 182-300_ March 2021-July 2021)/251d Nokia Audio themes - Car videoringtone.aac
215. `ERUNWAY`	Season 4 (days 301-present_ July 2021-present)/389n LG dLite - Runway.mp3
216. `NMYSOUL`	Season 2 (days 92-181_ December 2020-March 2021)/157n Sony Ericsson T700 - In my soul.m4a
217. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
218. `380GLOW`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow.aac
219. `ELSGOOD`	Season 4 (days 301-present_ July 2021-present)/320n Samsung Galaxy S20 - Feels Good.ogg
220. `DLEAVES`	Season 3 (days 182-300_ March 2021-July 2021)/235d HTC Touch HD - Leaves.wma
221. `ANDWINE`	Season 2 (days 92-181_ December 2020-March 2021)/108n LG KG280 - Cigar And Wine.flac
222. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
223. `INGWALK`	Season 4 (days 301-present_ July 2021-present)/302n LG Lotus - Evening Walk.mp3
224. `0CIRCUS`	Season 3 (days 182-300_ March 2021-July 2021)/188d Motorola RIZR Z10 - Circus.mp3
225. `10GROWL`	Season 2 (days 92-181_ December 2020-March 2021)/107n Android 1.0 - Growl.ogg
226. `HANGING`	Season 4 (days 301-present_ July 2021-present)/315d Motorola A3100 - Hanging.mp3
227. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
228. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
229. `INLOOPS`	Season 1 (days 1-91_ September 2020-December 2020)/74 Motorola RAZR V3c - Latin Loops.mp3
230. `ONDDAWN`	Season 4 (days 301-present_ July 2021-present)/322d HTC Touch Diamond - Dawn.wav
231. `NRAMBLE`	Season 4 (days 301-present_ July 2021-present)/331d Alcatel OT-660 - Urban Ramble.mp3
232. `ARNIVAL`	Season 3 (days 182-300_ March 2021-July 2021)/284d Samsung Galaxy S20 - Carnival.ogg
233. `START~2`	Season 4 (days 301-present_ July 2021-present)/380de Mandriva Linux 2006 - Startup.wav
234. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.mp3
235. `CECRAFT`	Season 3 (days 182-300_ March 2021-July 2021)/221n Samsung Galaxy S7 - Spacecraft.ogg
236. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
237. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
238. `2RHODES`	Season 2 (days 92-181_ December 2020-March 2021)/95n HTC E-Club Ring Tone Pack 2 - Rhodes.MP3
239. `BEANICE`	Season 2 (days 92-181_ December 2020-March 2021)/101d Android 1.0 - Caribbean Ice.ogg
240. `98FLUID`	Season 3 (days 182-300_ March 2021-July 2021)/260d Motorola E398 - Fluid.mp3
241. `NIVERSE`	Season 1 (days 1-91_ September 2020-December 2020)/45 Motorola SLVR L6g (Chinese version) - Little Universe.mp3
242. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
243. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
244. `T_TTONE`	Season 2 (days 92-181_ December 2020-March 2021)/130n AT_T - AT_T Tone.wav
245. `UNBEAMZ`	Season 3 (days 182-300_ March 2021-July 2021)/232n Sony Ericsson Vivaz - Sunbeamz.m4a
246. `OS7SILK`	Season 1 (days 1-91_ September 2020-December 2020)/76 iOS 7 - Silk.m4r
247. `LSIGNAL`	Season 1 (days 1-91_ September 2020-December 2020)/36 Motorola W490 - Digital Signal.mp3
248. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
249. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
250. `ERIMENT`	Season 4 (days 301-present_ July 2021-present)/347n Nokia N9 - Noise experiment.mp3
251. `NKY_ALL`	Season 4 (days 301-present_ July 2021-present)/302d Android 1.5 Cupcake - Funk Y_all.wav
252. `UNLEASH`	Season 2 (days 92-181_ December 2020-March 2021)/110d Motorola Q9c - Unleash.mp3
253. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav
254. `PDRAGON`	Season 2 (days 92-181_ December 2020-March 2021)/165d Nokia 7710 - Snapdragon.aac

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
