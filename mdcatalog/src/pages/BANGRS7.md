---
title: BANGRS7
date: "2021-12-17"
---

# BANGRS7

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `T_TTONE`	Season 2 (days 92-181_ December 2020-March 2021)/130n AT_T - AT_T Tone.wav
3. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3
4. `000MOON`	Season 4 (days 301-present_ July 2021-present)/391d Motorola A1000 - Moon.mp3
5. `90WAVES`	Season 2 (days 92-181_ December 2020-March 2021)/135d Motorola C390 - Waves.mp3
6. `SEASIDE`	Season 1 (days 1-91_ September 2020-December 2020)/26 iOS 7 - By The Seaside.m4r
7. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
8. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
9. `5MINGLE`	Season 2 (days 92-181_ December 2020-March 2021)/125d Nokia N85 - Mingle.aac
10. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
11. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
12. `ROOVING`	Season 3 (days 182-300_ March 2021-July 2021)/227d Motorola M702ig - Grooving.3gp
13. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
14. `INGTO~1`	Season 3 (days 182-300_ March 2021-July 2021)/256d Nokia Audio themes - Car ringtone.aac
15. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
16. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
17. `AYDREAM`	Season 3 (days 182-300_ March 2021-July 2021)/262d Windows Mobile Ring Tone Pack 5 - Day Dream.wma
18. `ARNIVAL`	Season 3 (days 182-300_ March 2021-July 2021)/284d Samsung Galaxy S20 - Carnival.ogg
19. `CECRAFT`	Season 3 (days 182-300_ March 2021-July 2021)/221n Samsung Galaxy S7 - Spacecraft.ogg
20. `CEPTRUM`	Season 3 (days 182-300_ March 2021-July 2021)/265n Android 2.3 Gingerbread - Sceptrum.ogg
21. `VOLTAGE`	Season 1 (days 1-91_ September 2020-December 2020)/59 BenQ-Siemens S68 - High Voltage.wav
22. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
23. `RE2KICK`	Season 4 (days 301-present_ July 2021-present)/359n Motorola ROKR E2 - Kick.mp3
24. `LSIGNAL`	Season 1 (days 1-91_ September 2020-December 2020)/36 Motorola W490 - Digital Signal.mp3
25. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
26. `OOKBACK`	Season 3 (days 182-300_ March 2021-July 2021)/185n Samsung B5702 DuoS - Look Back.mp3
27. `INGTO~2`	Season 3 (days 182-300_ March 2021-July 2021)/266n Nokia Audio themes - Jazz ringtone.aac
28. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
29. `55BRILL`	Season 3 (days 182-300_ March 2021-July 2021)/257d Nokia E55 - Brill.aac
30. `INGWALK`	Season 4 (days 301-present_ July 2021-present)/302n LG Lotus - Evening Walk.mp3
31. `0GLOWHQ`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow (HQ).wav
32. `RL7FIRE`	Season 2 (days 92-181_ December 2020-March 2021)/100d Motorola SLVR L7 - Fire.m4a
33. `DOSCOPE`	Season 2 (days 92-181_ December 2020-March 2021)/92n Motorola E365 - Kaleidoscope.flac
34. `SCOVERY`	Season 2 (days 92-181_ December 2020-March 2021)/148n Nokia 8600 - Discovery.aac
35. `TEPPERS`	Season 3 (days 182-300_ March 2021-July 2021)/246n Panasonic G50 - Steppers.wav
36. `60GREEN`	Season 4 (days 301-present_ July 2021-present)/313d Alcatel OT-660 - Green.mp3
37. `NIVERSE`	Season 1 (days 1-91_ September 2020-December 2020)/45 Motorola SLVR L6g (Chinese version) - Little Universe.mp3
38. `SSWIVIT`	Season 3 (days 182-300_ March 2021-July 2021)/290n Android 2.0 Eclair - Don_ Mess Wiv It.wav
39. `SYBREAK`	Season 3 (days 182-300_ March 2021-July 2021)/267d HTC E-Club Ring Tone Pack 2 - Bluesy Break.MP3
40. `LOTHEME`	Season 3 (days 182-300_ March 2021-July 2021)/223n Windows Mobile (downloadable) - Halo Theme.mp3
41. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
42. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
43. `OVATION`	Season 2 (days 92-181_ December 2020-March 2021)/127d HTC E-Club Ring Tone Pack 1 - HTC Innovation.MP3
44. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
45. `ALKAWAY`	Season 3 (days 182-300_ March 2021-July 2021)/258d Nokia N85 - Walk away.aac
46. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
47. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.mp3
48. `97BEATS`	Season 1 (days 1-91_ September 2020-December 2020)/33 Motorola V197 - Beats.mp3
49. `ANORIFF`	Season 3 (days 182-300_ March 2021-July 2021)/238d iPhone - Piano Riff.m4a
50. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
51. `UNBEAMZ`	Season 3 (days 182-300_ March 2021-July 2021)/232n Sony Ericsson Vivaz - Sunbeamz.m4a
52. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
53. `NCOFIRE`	Season 2 (days 92-181_ December 2020-March 2021)/158d Windows Mobile Ring Tone Pack 2 - Flamenco Fire.wma
54. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
55. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
56. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
57. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
58. `Q9HNOVA`	Season 4 (days 301-present_ July 2021-present)/301n Motorola Q9h - Nova.mp3
59. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
60. `10GROWL`	Season 2 (days 92-181_ December 2020-March 2021)/107n Android 1.0 - Growl.ogg
61. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
62. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
63. `REVERIE`	Season 2 (days 92-181_ December 2020-March 2021)/145d Samsung Ego - Reverie.mp3
64. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
65. `ENDANCE`	Season 4 (days 301-present_ July 2021-present)/317n HP iPAQ Glisten - Dance.mp3
66. `ROHOUSE`	Season 3 (days 182-300_ March 2021-July 2021)/296d Samsung PC Studio - Euro House.wav
67. `LEFEVER`	Season 4 (days 301-present_ July 2021-present)/355d LG KG800 Chocolate - Jungle Fever.wav
68. `K3SHINE`	Season 2 (days 92-181_ December 2020-March 2021)/164d Motorola KRZR K3 - Shine.mp3
69. `EADNEON`	Season 1 (days 1-91_ September 2020-December 2020)/79 Android 2.3 Gingerbread - Neon.wav
70. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
71. `00BC110`	Season 2 (days 92-181_ December 2020-March 2021)/142d Nokia 8800 - Bc110.aac
72. `6LIQUID`	Season 3 (days 182-300_ March 2021-July 2021)/183n Motorola SLVR L6 - Liquid.mp3
73. `NVASION`	Season 4 (days 301-present_ July 2021-present)/330d Motorola Droid A855 - Droid Invasion.ogg
74. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
75. `HOLIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/116n Samsung Galaxy S8 - Holiday.ogg
76. `POPXRAY`	Season 4 (days 301-present_ July 2021-present)/382n Alcatel M_POP - X-Ray.mp3
77. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
78. `UNNYDAY`	Season 2 (days 92-181_ December 2020-March 2021)/103d Samsung C5212 - Sunny day.mp3
79. `YS8AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/198n Samsung Galaxy S8 - Aqua.ogg
80. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
81. `RASONIC`	Season 1 (days 1-91_ September 2020-December 2020)/83 Motorola E398 - Ultra Sonic.mp3
82. `ITCOMES`	Season 3 (days 182-300_ March 2021-July 2021)/268d Samsung SGH-i400 - Here it comes.mp3
83. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
84. `ELSGOOD`	Season 4 (days 301-present_ July 2021-present)/320n Samsung Galaxy S20 - Feels Good.ogg
85. `98FLUID`	Season 3 (days 182-300_ March 2021-July 2021)/260d Motorola E398 - Fluid.mp3
86. `CHHOUSE`	Season 2 (days 92-181_ December 2020-March 2021)/99d Sony Ericsson T700 - Beach house.m4a
87. `ECIRCUS`	Season 4 (days 301-present_ July 2021-present)/362n Motorola A3100 - Space Circus.mp3
88. `ERWORLD`	Season 3 (days 182-300_ March 2021-July 2021)/209d Samsung Galaxy S III - Underwater world.ogg
89. `CEPARTY`	Season 1 (days 1-91_ September 2020-December 2020)/72 Samsung Galaxy S6 - Dance Party.ogg
90. `HRIDING`	Season 3 (days 182-300_ March 2021-July 2021)/288n Samsung Ego - Smooth riding.mp3
91. `ERUNWAY`	Season 4 (days 301-present_ July 2021-present)/389n LG dLite - Runway.mp3
92. `0CIRCUS`	Season 3 (days 182-300_ March 2021-July 2021)/188d Motorola RIZR Z10 - Circus.mp3
93. `AMBIENT`	Season 3 (days 182-300_ March 2021-July 2021)/292n Motorola RAZR V3i - Ambient.mp3
94. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
95. `LSTREET`	Season 4 (days 301-present_ July 2021-present)/332d Sony Ericsson P1i - Hill street.mp3
96. `KIATUNE`	Season 2 (days 92-181_ December 2020-March 2021)/126n Nokia 6270 - Nokia tune.aac
97. `HANGING`	Season 4 (days 301-present_ July 2021-present)/315d Motorola A3100 - Hanging.mp3
98. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
99. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
100. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
101. `RADIATE`	Season 2 (days 92-181_ December 2020-March 2021)/149n iOS 7 - Radiate.m4r
102. `Q9HGLOW`	Season 4 (days 301-present_ July 2021-present)/357n Motorola Q9h - Glow.mp3
103. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
104. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
105. `HETRICK`	Season 3 (days 182-300_ March 2021-July 2021)/255d Nokia 6500 - The trick.aac
106. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
107. `BYWALTZ`	Season 3 (days 182-300_ March 2021-July 2021)/206n3 Samsung Galaxy S20 - Baby Waltz.ogg
108. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
109. `OS7SILK`	Season 1 (days 1-91_ September 2020-December 2020)/76 iOS 7 - Silk.m4r
110. `CSLEAZY`	Season 1 (days 1-91_ September 2020-December 2020)/10 Motorola E1000 - Mcsleazy.mp3
111. `LIDSODA`	Season 2 (days 92-181_ December 2020-March 2021)/107d Yamaha SMAF (downloadable) - Solid Soda.flac
112. `LATINUM`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
113. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
114. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380dh Ubuntu 6.10 - Startup.wav
115. `ERSURGE`	Season 2 (days 92-181_ December 2020-March 2021)/134n Motorola E1070 - Power Surge.mp3
116. `UNLEASH`	Season 2 (days 92-181_ December 2020-March 2021)/110d Motorola Q9c - Unleash.mp3
117. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
118. `DEMO021`	Season 3 (days 182-300_ March 2021-July 2021)/293d Yamaha SMAF (downloadable) - MA Sound Demo021.wav
119. `GHSCORE`	Season 2 (days 92-181_ December 2020-March 2021)/151d Windows Mobile Extended Audio Pack 1 - High Score.wma
120. `5RUNWAY`	Season 4 (days 301-present_ July 2021-present)/350d Nokia N85 - Runway.wma
121. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
122. `INAMASK`	Season 4 (days 301-present_ July 2021-present)/367d Motorola V191 (China) - Mask.mp3
123. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
124. `NKY_ALL`	Season 4 (days 301-present_ July 2021-present)/302d Android 1.5 Cupcake - Funk Y_all.wav
125. `BOOKEM_`	Season 2 (days 92-181_ December 2020-March 2021)/143n HTC HD7 - Book Em_.mp3
126. `ERSPACE`	Season 2 (days 92-181_ December 2020-March 2021)/175d Nokia 6108 - Hyperspace.flac
127. `RGOTCHA`	Season 3 (days 182-300_ March 2021-July 2021)/219d Android 2.0 Eclair - Gotcha.wav
128. `ACKHOLE`	Season 4 (days 301-present_ July 2021-present)/387d Nokia Tones - Black Hole.wma
129. `WFLAKES`	Season 2 (days 92-181_ December 2020-March 2021)/115d Nokia 6681 - Snowflakes.flac
130. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
131. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
132. `6WAHWAH`	Season 3 (days 182-300_ March 2021-July 2021)/287n Nokia 2626 - Wah wah.mp3
133. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
134. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
135. `NICBOOM`	Season 2 (days 92-181_ December 2020-March 2021)/97d Samsung Gravity SGH-T456 - Sonicboom.mp3
136. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
137. `TARTUP3`	Season 4 (days 301-present_ July 2021-present)/380dd KDE 3.3 - Startup 3.ogg
138. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
139. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
140. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
141. `NODANCE`	Season 3 (days 182-300_ March 2021-July 2021)/192d Samsung SGH-P250 - Techno dance.mp3
142. `LITHAZE`	Season 4 (days 301-present_ July 2021-present)/338d Motorola E815 - Moonlit Haze.wav
143. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
144. `INLOOPS`	Season 1 (days 1-91_ September 2020-December 2020)/74 Motorola RAZR V3c - Latin Loops.mp3
145. `SODAPOP`	Season 3 (days 182-300_ March 2021-July 2021)/248d Nokia 6300 (newer firmware) - Soda pop.aac
146. `LDSTYLE`	Season 2 (days 92-181_ December 2020-March 2021)/96n Motorola RIZR Z3 - Wild Style.mp3
147. `KINGDOM`	Season 3 (days 182-300_ March 2021-July 2021)/184d Panasonic G60 - 808Kingdom.wav
148. `START~1`	Season 4 (days 301-present_ July 2021-present)/380df Mandriva Linux 2007 - Startup.wav
149. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
150. `ITRAILS`	Season 4 (days 301-present_ July 2021-present)/378n Nokia 5140i - Trails.wma
151. `DYNAMIC`	Season 3 (days 182-300_ March 2021-July 2021)/224n Motorola RAZR V3xx - Dynamic.mp3
152. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
153. `95GLINT`	Season 3 (days 182-300_ March 2021-July 2021)/216n Nokia N95 - Glint.aac
154. `1ORANGE`	Season 1 (days 1-91_ September 2020-December 2020)/67 Xiaomi Mi A1 - Orange.ogg
155. `0SUNSET`	Season 2 (days 92-181_ December 2020-March 2021)/160n Motorola A1000 - Sunset.mp3
156. `OCOLATE`	Season 2 (days 92-181_ December 2020-March 2021)/115n Windows Mobile Ring Tone Pack 3 - Hot Chocolate.wma
157. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
158. `TENIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/203n Sony Ericsson T700 - Late night.m4a
159. `0AMINOR`	Season 3 (days 182-300_ March 2021-July 2021)/197n LG S5100 - A minor.wav
160. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
161. `EFLIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/136n Android 2.0 Eclair - Free Flight.wav
162. `START~2`	Season 4 (days 301-present_ July 2021-present)/380dg Ubuntu 4.10 - Startup.wav
163. `ENALINE`	Season 3 (days 182-300_ March 2021-July 2021)/195d Motorola KRZR K1 - Adrenaline.mp3
164. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
165. `OCKTAIL`	Season 2 (days 92-181_ December 2020-March 2021)/142n Motorola RAZR V3i - Cocktail.mp3
166. `HARDMIX`	Season 3 (days 182-300_ March 2021-July 2021)/288d Windows Mobile Ring Tone Pack 5 - Hard Mix.wma
167. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
168. `GTONE17`	Season 3 (days 182-300_ March 2021-July 2021)/277n LG enV2 - Ringtone 17.wav
169. `EDITION`	Season 3 (days 182-300_ March 2021-July 2021)/195n Android 1.5 Cupcake - Champagne Edition.wav
170. `WFLAK~1`	Season 2 (days 92-181_ December 2020-March 2021)/113d Windows Mobile Ring Tone Pack 3 - Snow Flakes.wma
171. `YFUTURE`	Season 3 (days 182-300_ March 2021-July 2021)/260n Sony Ericsson Vivaz - X-ray future.m4a
172. `DEMO041`	Season 3 (days 182-300_ March 2021-July 2021)/190n Yamaha SMAF (downloadable) - MA Sound Demo041.wav
173. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
174. `OLAQDOT`	Season 4 (days 301-present_ July 2021-present)/385n Motorola Q - Dot.mp3
175. `ERDISCO`	Season 2 (days 92-181_ December 2020-March 2021)/140d Samsung Galaxy S20 - Roller Disco.ogg
176. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
177. `NFORMER`	Season 3 (days 182-300_ March 2021-July 2021)/247n HTC One M9 - Informer.mp3
178. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
179. `IDPLAYA`	Season 1 (days 1-91_ September 2020-December 2020)/41 Android - Playa.wav
180. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
181. `START~3`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
182. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
183. `BAGITUP`	Season 1 (days 1-91_ September 2020-December 2020)/11 BenQ-Siemens S68 - Bag it up.wav
184. `HORIZON`	Season 2 (days 92-181_ December 2020-March 2021)/128n Samsung Galaxy S II - Over the horizon.ogg
185. `LATIN~1`	Season 3 (days 182-300_ March 2021-July 2021)/272d Motorola Q9m - Platinum.mp3
186. `ICDISCO`	Season 4 (days 301-present_ July 2021-present)/390d Microsoft Office clipart - Generic Disco.wav
187. `GETAWAY`	Season 2 (days 92-181_ December 2020-March 2021)/161d Google Pixel 4 - Getaway.ogg
188. `HTSWING`	Season 4 (days 301-present_ July 2021-present)/365n Samsung Galaxy S9 - Midnight Swing.ogg
189. `90BIKER`	Season 2 (days 92-181_ December 2020-March 2021)/119d Motorola C390 - Biker.mp3
190. `GFUSION`	Season 3 (days 182-300_ March 2021-July 2021)/209n Motorola M702ig - Fusion.3gp
191. `9FRIDAY`	Season 1 (days 1-91_ September 2020-December 2020)/39 Samsung Galaxy S9 - Friday.ogg
192. `OULMATE`	Season 4 (days 301-present_ July 2021-present)/351n Samsung SGH-D980 DUOS - Melody5 (Soul mate).wav
193. `GTONE01`	Season 1 (days 1-91_ September 2020-December 2020)/42 Windows 7 - Ringtone 01.wma
194. `GHTLIFE`	Season 4 (days 301-present_ July 2021-present)/308n GFive G9 - Night Life.ogg
195. `2RHODES`	Season 2 (days 92-181_ December 2020-March 2021)/95n HTC E-Club Ring Tone Pack 2 - Rhodes.MP3
196. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
197. `SEDRIVE`	Season 4 (days 301-present_ July 2021-present)/335d fusoxide_s Ringtones Vol. 1 - House Drive.wav
198. `SURGENT`	Season 4 (days 301-present_ July 2021-present)/354n Sonic Logos, Links and Beds (Album 2) - It_s Urgent.mp3
199. `QBOOGIE`	Season 4 (days 301-present_ July 2021-present)/383n Windows Mobile (downloadable) - BBQ Boogie.wma
200. `THEBEAT`	Season 2 (days 92-181_ December 2020-March 2021)/117d Windows Mobile Ring Tone Pack 8 - Carol Of The Beat.wma
201. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
202. `OVIOLET`	Season 3 (days 182-300_ March 2021-July 2021)/271d Nokia Oro - Violet.aac
203. `AYNIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/152n Motorola E398 - Saturday night.mp3
204. `EDUBLIN`	Season 4 (days 301-present_ July 2021-present)/363d T-Mobile Sidekick Slide - Dublin.mp3
205. `TICTONE`	Season 1 (days 1-91_ September 2020-December 2020)/28 Samsung Galaxy S5 - Mystic Tone.ogg
206. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
207. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
208. `LLREMIX`	Season 1 (days 1-91_ September 2020-December 2020)/63 Discord - Incoming Call (Remix).mp3
209. `310KICK`	Season 4 (days 301-present_ July 2021-present)/340d Nokia 3310 - Kick.wma
210. `FUNTIME`	Season 4 (days 301-present_ July 2021-present)/339d LG CE110 - Fun-Time.mp3
211. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
212. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
213. `IGHTIDE`	Season 4 (days 301-present_ July 2021-present)/373d Samsung Galaxy S III - High tide.ogg
214. `NEALARM`	Season 4 (days 301-present_ July 2021-present)/328n iPhone - Alarm.m4a
215. `OPENING`	Season 2 (days 92-181_ December 2020-March 2021)/129n iOS 7 - Opening.m4r
216. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
217. `CLASSIC`	Season 2 (days 92-181_ December 2020-March 2021)/132n Windows Phone 8 - Classic.wma
218. `ORGANIC`	Season 3 (days 182-300_ March 2021-July 2021)/191d HTC Touch - Organic.wma
219. `MINATED`	Season 1 (days 1-91_ September 2020-December 2020)/22 Android - Terminated.ogg
220. `IGHTOWL`	Season 2 (days 92-181_ December 2020-March 2021)/175n iOS 7 - Night Owl.m4r
221. `ONDDAWN`	Season 4 (days 301-present_ July 2021-present)/322d HTC Touch Diamond - Dawn.wav
222. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
223. `THETONE`	Season 4 (days 301-present_ July 2021-present)/306n Android 0.9 - Romancing The Tone.ogg
224. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
225. `LLYWOOD`	Season 3 (days 182-300_ March 2021-July 2021)/208d Android 1.5 Cupcake - Bollywood.wav
226. `0DAMMAR`	Season 4 (days 301-present_ July 2021-present)/327n HTC 10 - Dammar.wav
227. `TIMEPAD`	Season 4 (days 301-present_ July 2021-present)/358d Alcatel OT-660 - Time Pad.mp3
228. `CLAPPER`	Season 4 (days 301-present_ July 2021-present)/346d Nokia N78 - Clapper.mp4
229. `NEWFLOW`	Season 1 (days 1-91_ September 2020-December 2020)/14 Motorola RAZR V3i - New Flow.mp3
230. `SCRAPER`	Season 2 (days 92-181_ December 2020-March 2021)/98n Samsung Galaxy S8 - Skyscraper.ogg
231. `THEDARK`	Season 4 (days 301-present_ July 2021-present)/358n Samsung SGH-Z130 - Dancing in the Dark.mp3
232. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
233. `START~4`	Season 4 (days 301-present_ July 2021-present)/380de Mandriva Linux 2006 - Startup.wav
234. `EAKDOWN`	Season 4 (days 301-present_ July 2021-present)/377n Motorola RAZR V3i - Breakdown.mp3
235. `HICBABA`	Season 4 (days 301-present_ July 2021-present)/312n Samsung Galaxy Pocket - Chic baba.ogg
236. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
237. `IDHENRY`	Season 4 (days 301-present_ July 2021-present)/344n Gionee GN305 - Horrid Henry.mp3
238. `ALIENTE`	Season 1 (days 1-91_ September 2020-December 2020)/75 Windows Mobile (downloadable) - Ballroom Caliente.wma
239. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
240. `TARTUP2`	Season 4 (days 301-present_ July 2021-present)/380dc KDE 3.3 - Startup 2.ogg
241. `OFORION`	Season 2 (days 92-181_ December 2020-March 2021)/169d BenQ-Siemens E71 - Bells of Orion.wav
242. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
243. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
244. `DEMO055`	Season 3 (days 182-300_ March 2021-July 2021)/282d Yamaha SMAF (downloadable) - MA Sound Demo055.wav
245. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
246. `NGTONE7`	Season 1 (days 1-91_ September 2020-December 2020)/23 Windows 7 Beta - Ringtone 7.wma
247. `LLBLEND`	Season 2 (days 92-181_ December 2020-March 2021)/152d Android 1.0 - Curve Ball Blend.ogg
248. `CATWALK`	Season 3 (days 182-300_ March 2021-July 2021)/184n Nokia 5300 - Catwalk.aac
249. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
250. `GONAVIS`	Season 3 (days 182-300_ March 2021-July 2021)/202n Android 2.3 Gingerbread - Argo Navis.ogg
251. `0VISION`	Season 3 (days 182-300_ March 2021-July 2021)/233d Windows Mobile 6.0 - Vision.wma
252. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
253. `ERIMENT`	Season 4 (days 301-present_ July 2021-present)/347n Nokia N9 - Noise experiment.mp3
254. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
