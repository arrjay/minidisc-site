---
title: BANGRS4
date: "2021-12-16"
---

# BANGRS4

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `OADTRIP`	Season 2 (days 92-181_ December 2020-March 2021)/146d Android 1.5 Cupcake - Road Trip.wav
3. `1ORANGE`	Season 1 (days 1-91_ September 2020-December 2020)/67 Xiaomi Mi A1 - Orange.ogg
4. `NICBOOM`	Season 2 (days 92-181_ December 2020-March 2021)/97d Samsung Gravity SGH-T456 - Sonicboom.mp3
5. `OULMATE`	Season 4 (days 301-present_ July 2021-present)/351n Samsung SGH-D980 DUOS - Melody5 (Soul mate).wav
6. `IGHTOWL`	Season 2 (days 92-181_ December 2020-March 2021)/175n iOS 7 - Night Owl.m4r
7. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
8. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
9. `INGTO~1`	Season 4 (days 301-present_ July 2021-present)/319d Nokia Audio themes - Golf ringtone.aac
10. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
11. `KINGDOM`	Season 3 (days 182-300_ March 2021-July 2021)/184d Panasonic G60 - 808Kingdom.wav
12. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
13. `0GLOWHQ`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow (HQ).wav
14. `POPXRAY`	Season 4 (days 301-present_ July 2021-present)/382n Alcatel M_POP - X-Ray.mp3
15. `ULLMOON`	Season 1 (days 1-91_ September 2020-December 2020)/61 Windows Mobile Ring Tone Pack 6 - Full Moon.wma
16. `SYBREAK`	Season 3 (days 182-300_ March 2021-July 2021)/267d HTC E-Club Ring Tone Pack 2 - Bluesy Break.MP3
17. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
18. `ICDISCO`	Season 4 (days 301-present_ July 2021-present)/390d Microsoft Office clipart - Generic Disco.wav
19. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
20. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
21. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
22. `OCKTAIL`	Season 2 (days 92-181_ December 2020-March 2021)/142n Motorola RAZR V3i - Cocktail.mp3
23. `CSLEAZY`	Season 1 (days 1-91_ September 2020-December 2020)/10 Motorola E1000 - Mcsleazy.mp3
24. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
25. `YFUTURE`	Season 3 (days 182-300_ March 2021-July 2021)/260n Sony Ericsson Vivaz - X-ray future.m4a
26. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
27. `BERBAND`	Season 1 (days 1-91_ September 2020-December 2020)/38 Windows Mobile (downloadable) - Rubber Band.wma
28. `INGTO~2`	Season 3 (days 182-300_ March 2021-July 2021)/256d Nokia Audio themes - Car ringtone.aac
29. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
30. `UNBEAMZ`	Season 3 (days 182-300_ March 2021-July 2021)/232n Sony Ericsson Vivaz - Sunbeamz.m4a
31. `START~1`	Season 4 (days 301-present_ July 2021-present)/380df Mandriva Linux 2007 - Startup.wav
32. `TICTONE`	Season 1 (days 1-91_ September 2020-December 2020)/28 Samsung Galaxy S5 - Mystic Tone.ogg
33. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
34. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
35. `OFORION`	Season 2 (days 92-181_ December 2020-March 2021)/169d BenQ-Siemens E71 - Bells of Orion.wav
36. `0GROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/149d Motorola A1000 - Groove.mp3
37. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
38. `ACKDIVE`	Season 3 (days 182-300_ March 2021-July 2021)/230n Samsung Jack - Dive.mp3
39. `OOKBACK`	Season 3 (days 182-300_ March 2021-July 2021)/185n Samsung B5702 DuoS - Look Back.mp3
40. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
41. `0DAMMAR`	Season 4 (days 301-present_ July 2021-present)/327n HTC 10 - Dammar.wav
42. `90WAVES`	Season 2 (days 92-181_ December 2020-March 2021)/135d Motorola C390 - Waves.mp3
43. `ORGANIC`	Season 3 (days 182-300_ March 2021-July 2021)/191d HTC Touch - Organic.wma
44. `HARDMIX`	Season 3 (days 182-300_ March 2021-July 2021)/288d Windows Mobile Ring Tone Pack 5 - Hard Mix.wma
45. `ROHOUSE`	Season 3 (days 182-300_ March 2021-July 2021)/296d Samsung PC Studio - Euro House.wav
46. `ASTWARD`	Season 4 (days 301-present_ July 2021-present)/343n Alcatel OT-708X - Eastward.mp3
47. `QBOOGIE`	Season 4 (days 301-present_ July 2021-present)/383n Windows Mobile (downloadable) - BBQ Boogie.wma
48. `LDSTYLE`	Season 2 (days 92-181_ December 2020-March 2021)/96n Motorola RIZR Z3 - Wild Style.mp3
49. `RW5COOL`	Season 3 (days 182-300_ March 2021-July 2021)/217n Motorola ROKR W5 - Cool.mp3
50. `AMBIENT`	Season 3 (days 182-300_ March 2021-July 2021)/292n Motorola RAZR V3i - Ambient.mp3
51. `CEPARTY`	Season 1 (days 1-91_ September 2020-December 2020)/72 Samsung Galaxy S6 - Dance Party.ogg
52. `DOSCOPE`	Season 2 (days 92-181_ December 2020-March 2021)/92n Motorola E365 - Kaleidoscope.flac
53. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
54. `LSTREET`	Season 4 (days 301-present_ July 2021-present)/332d Sony Ericsson P1i - Hill street.mp3
55. `REVERIE`	Season 2 (days 92-181_ December 2020-March 2021)/145d Samsung Ego - Reverie.mp3
56. `BYWALTZ`	Season 3 (days 182-300_ March 2021-July 2021)/206n3 Samsung Galaxy S20 - Baby Waltz.ogg
57. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
58. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
59. `THETONE`	Season 4 (days 301-present_ July 2021-present)/306n Android 0.9 - Romancing The Tone.ogg
60. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
61. `INLOOPS`	Season 1 (days 1-91_ September 2020-December 2020)/74 Motorola RAZR V3c - Latin Loops.mp3
62. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
63. `GHSCORE`	Season 2 (days 92-181_ December 2020-March 2021)/151d Windows Mobile Extended Audio Pack 1 - High Score.wma
64. `RADIATE`	Season 2 (days 92-181_ December 2020-March 2021)/149n iOS 7 - Radiate.m4r
65. `BEANICE`	Season 2 (days 92-181_ December 2020-March 2021)/101d Android 1.0 - Caribbean Ice.ogg
66. `GHTLIFE`	Season 4 (days 301-present_ July 2021-present)/308n GFive G9 - Night Life.ogg
67. `ULTTONE`	Season 4 (days 301-present_ July 2021-present)/342n LG Venus - VZW Default Tone.mp3
68. `ITCOMES`	Season 3 (days 182-300_ March 2021-July 2021)/268d Samsung SGH-i400 - Here it comes.mp3
69. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
70. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
71. `HRIDING`	Season 3 (days 182-300_ March 2021-July 2021)/288n Samsung Ego - Smooth riding.mp3
72. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
73. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
74. `HTSWING`	Season 4 (days 301-present_ July 2021-present)/365n Samsung Galaxy S9 - Midnight Swing.ogg
75. `CEPTRUM`	Season 3 (days 182-300_ March 2021-July 2021)/265n Android 2.3 Gingerbread - Sceptrum.ogg
76. `ERSURGE`	Season 2 (days 92-181_ December 2020-March 2021)/134n Motorola E1070 - Power Surge.mp3
77. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
78. `GHTCLUB`	Season 3 (days 182-300_ March 2021-July 2021)/262n Motorola ROKR Z6 - Nightclub.mp3
79. `INAMASK`	Season 4 (days 301-present_ July 2021-present)/367d Motorola V191 (China) - Mask.mp3
80. `UNNYDAY`	Season 2 (days 92-181_ December 2020-March 2021)/103d Samsung C5212 - Sunny day.mp3
81. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
82. `370CLUB`	Season 2 (days 92-181_ December 2020-March 2021)/136d Nokia 7370 - Club.3gp
83. `DEMO055`	Season 3 (days 182-300_ March 2021-July 2021)/282d Yamaha SMAF (downloadable) - MA Sound Demo055.wav
84. `0SUNSET`	Season 2 (days 92-181_ December 2020-March 2021)/160n Motorola A1000 - Sunset.mp3
85. `WEETPIE`	Season 4 (days 301-present_ July 2021-present)/334d LG Viewty Smart - Sweet Pie.mp3
86. `10GROWL`	Season 2 (days 92-181_ December 2020-March 2021)/107n Android 1.0 - Growl.ogg
87. `SURGENT`	Season 4 (days 301-present_ July 2021-present)/354n Sonic Logos, Links and Beds (Album 2) - It_s Urgent.mp3
88. `ERDISCO`	Season 2 (days 92-181_ December 2020-March 2021)/140d Samsung Galaxy S20 - Roller Disco.ogg
89. `TEPPERS`	Season 3 (days 182-300_ March 2021-July 2021)/246n Panasonic G50 - Steppers.wav
90. `NIVERSE`	Season 1 (days 1-91_ September 2020-December 2020)/45 Motorola SLVR L6g (Chinese version) - Little Universe.mp3
91. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
92. `PDRAGON`	Season 2 (days 92-181_ December 2020-March 2021)/165d Nokia 7710 - Snapdragon.aac
93. `LAQRIFF`	Season 4 (days 301-present_ July 2021-present)/342d Motorola Q - Riff.mp3
94. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
95. `DEMO044`	Season 3 (days 182-300_ March 2021-July 2021)/244d Yamaha SMAF (downloadable) - MA Sound Demo044.wav
96. `LLYWOOD`	Season 3 (days 182-300_ March 2021-July 2021)/208d Android 1.5 Cupcake - Bollywood.wav
97. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
98. `ECIRCUS`	Season 4 (days 301-present_ July 2021-present)/362n Motorola A3100 - Space Circus.mp3
99. `LATINUM`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
100. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
101. `BAGITUP`	Season 1 (days 1-91_ September 2020-December 2020)/11 BenQ-Siemens S68 - Bag it up.wav
102. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
103. `ONDDAWN`	Season 4 (days 301-present_ July 2021-present)/322d HTC Touch Diamond - Dawn.wav
104. `ANDWINE`	Season 2 (days 92-181_ December 2020-March 2021)/108n LG KG280 - Cigar And Wine.flac
105. `NVASION`	Season 4 (days 301-present_ July 2021-present)/330d Motorola Droid A855 - Droid Invasion.ogg
106. `SCRAPER`	Season 2 (days 92-181_ December 2020-March 2021)/98n Samsung Galaxy S8 - Skyscraper.ogg
107. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
108. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
109. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
110. `OPENING`	Season 2 (days 92-181_ December 2020-March 2021)/129n iOS 7 - Opening.m4r
111. `RL7FIRE`	Season 2 (days 92-181_ December 2020-March 2021)/100d Motorola SLVR L7 - Fire.m4a
112. `ROOVING`	Season 3 (days 182-300_ March 2021-July 2021)/227d Motorola M702ig - Grooving.3gp
113. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
114. `SCOVERY`	Season 2 (days 92-181_ December 2020-March 2021)/148n Nokia 8600 - Discovery.aac
115. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
116. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
117. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
118. `NEWFLOW`	Season 1 (days 1-91_ September 2020-December 2020)/14 Motorola RAZR V3i - New Flow.mp3
119. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
120. `LSIGNAL`	Season 1 (days 1-91_ September 2020-December 2020)/36 Motorola W490 - Digital Signal.mp3
121. `ELSGOOD`	Season 4 (days 301-present_ July 2021-present)/320n Samsung Galaxy S20 - Feels Good.ogg
122. `OS7SILK`	Season 1 (days 1-91_ September 2020-December 2020)/76 iOS 7 - Silk.m4r
123. `HORIZON`	Season 2 (days 92-181_ December 2020-March 2021)/128n Samsung Galaxy S II - Over the horizon.ogg
124. `RGOTCHA`	Season 3 (days 182-300_ March 2021-July 2021)/219d Android 2.0 Eclair - Gotcha.wav
125. `95GLINT`	Season 3 (days 182-300_ March 2021-July 2021)/216n Nokia N95 - Glint.aac
126. `ETSTYLE`	Season 4 (days 301-present_ July 2021-present)/380n Motorola W490 - Street Style.mp3
127. `ALIENTE`	Season 1 (days 1-91_ September 2020-December 2020)/75 Windows Mobile (downloadable) - Ballroom Caliente.wma
128. `KIDRIVE`	Season 4 (days 301-present_ July 2021-present)/337n HTC One X - Alki Drive.mp3
129. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
130. `REDIBLE`	Season 3 (days 182-300_ March 2021-July 2021)/280d Nokia 6270 - Elecredible.aac
131. `ESSENCE`	Season 3 (days 182-300_ March 2021-July 2021)/248n Motorola V551 - Essence.flac
132. `EFLIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/136n Android 2.0 Eclair - Free Flight.wav
133. `KCHIMER`	Season 1 (days 1-91_ September 2020-December 2020)/71 Windows Mobile Ring Tone Pack - Chimer.wma
134. `2PEPPER`	Season 3 (days 182-300_ March 2021-July 2021)/205n Nokia N72 - Pepper.aac
135. `GETAWAY`	Season 2 (days 92-181_ December 2020-March 2021)/161d Google Pixel 4 - Getaway.ogg
136. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
137. `5ROCKME`	Season 3 (days 182-300_ March 2021-July 2021)/235n Motorola ROKR EM35 - Rock Me.mp3
138. `ASTPACE`	Season 2 (days 92-181_ December 2020-March 2021)/155d Motorola Q9m - Fast Pace.mp3
139. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
140. `LLATION`	Season 3 (days 182-300_ March 2021-July 2021)/240n Motorola ROKR EM35 - Oscillation.mp3
141. `T_TTONE`	Season 2 (days 92-181_ December 2020-March 2021)/130n AT_T - AT_T Tone.wav
142. `AYDREAM`	Season 3 (days 182-300_ March 2021-July 2021)/262d Windows Mobile Ring Tone Pack 5 - Day Dream.wma
143. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
144. `SODAPOP`	Season 3 (days 182-300_ March 2021-July 2021)/248d Nokia 6300 (newer firmware) - Soda pop.aac
145. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.mp3
146. `00BELL6`	Season 1 (days 1-91_ September 2020-December 2020)/50 Samsung SGH-X100 - Bell 6.flac
147. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
148. `TELLITE`	Season 2 (days 92-181_ December 2020-March 2021)/94n Samsung Galaxy S10 - Satellite.ogg
149. `THEDARK`	Season 4 (days 301-present_ July 2021-present)/358n Samsung SGH-Z130 - Dancing in the Dark.mp3
150. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
151. `NRAMBLE`	Season 4 (days 301-present_ July 2021-present)/331d Alcatel OT-660 - Urban Ramble.mp3
152. `ERSPACE`	Season 2 (days 92-181_ December 2020-March 2021)/175d Nokia 6108 - Hyperspace.flac
153. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
154. `CLASSIC`	Season 2 (days 92-181_ December 2020-March 2021)/132n Windows Phone 8 - Classic.wma
155. `DLEAVES`	Season 3 (days 182-300_ March 2021-July 2021)/235d HTC Touch HD - Leaves.wma
156. `OUNGING`	Season 3 (days 182-300_ March 2021-July 2021)/276d Nokia 7370 - Lounging.wav
157. `ALKAWAY`	Season 3 (days 182-300_ March 2021-July 2021)/258d Nokia N85 - Walk away.aac
158. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
159. `PASSION`	Season 3 (days 182-300_ March 2021-July 2021)/198d Motorola E1000 - Passion.mp3
160. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
161. `ECLOUDS`	Season 4 (days 301-present_ July 2021-present)/310d Sony Ericsson P1i - Into the clouds.mp3
162. `INGTO~3`	Season 3 (days 182-300_ March 2021-July 2021)/251d Nokia Audio themes - Car videoringtone.aac
163. `TIMEPAD`	Season 4 (days 301-present_ July 2021-present)/358d Alcatel OT-660 - Time Pad.mp3
164. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
165. `Q9HGLOW`	Season 4 (days 301-present_ July 2021-present)/357n Motorola Q9h - Glow.mp3
166. `EAKDOWN`	Season 4 (days 301-present_ July 2021-present)/377n Motorola RAZR V3i - Breakdown.mp3
167. `SAFFRON`	Season 2 (days 92-181_ December 2020-March 2021)/154d HTC Ozone - Saffron.wma
168. `00BC110`	Season 2 (days 92-181_ December 2020-March 2021)/142d Nokia 8800 - Bc110.aac
169. `DAWNING`	Season 1 (days 1-91_ September 2020-December 2020)/37 Nokia 8800 Sirocco Edition - Dawning.aac
170. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
171. `SEASIDE`	Season 1 (days 1-91_ September 2020-December 2020)/26 iOS 7 - By The Seaside.m4r
172. `CLAPPER`	Season 4 (days 301-present_ July 2021-present)/346d Nokia N78 - Clapper.mp4
173. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
174. `ENDANCE`	Season 4 (days 301-present_ July 2021-present)/317n HP iPAQ Glisten - Dance.mp3
175. `OCOLATE`	Season 2 (days 92-181_ December 2020-March 2021)/115n Windows Mobile Ring Tone Pack 3 - Hot Chocolate.wma
176. `ILLASKY`	Season 1 (days 1-91_ September 2020-December 2020)/54 LG KG240 - Vanilla Sky.flac
177. `TRIPPER`	Season 4 (days 301-present_ July 2021-present)/387n HTC HD7 - Tripper.mp3
178. `MINATED`	Season 1 (days 1-91_ September 2020-December 2020)/22 Android - Terminated.ogg
179. `NGTONE7`	Season 1 (days 1-91_ September 2020-December 2020)/23 Windows 7 Beta - Ringtone 7.wma
180. `START~2`	Season 4 (days 301-present_ July 2021-present)/380dg Ubuntu 4.10 - Startup.wav
181. `ACKHOLE`	Season 4 (days 301-present_ July 2021-present)/387d Nokia Tones - Black Hole.wma
182. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
183. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
184. `GTONE01`	Season 1 (days 1-91_ September 2020-December 2020)/42 Windows 7 - Ringtone 01.wma
185. `Q9HNOVA`	Season 4 (days 301-present_ July 2021-present)/301n Motorola Q9h - Nova.mp3
186. `LIDSODA`	Season 2 (days 92-181_ December 2020-March 2021)/107d Yamaha SMAF (downloadable) - Solid Soda.flac
187. `SSWIVIT`	Season 3 (days 182-300_ March 2021-July 2021)/290n Android 2.0 Eclair - Don_ Mess Wiv It.wav
188. `FORWARD`	Season 4 (days 301-present_ July 2021-present)/303n T-Mobile Shadow - Moving Forward.wma
189. `ERWORLD`	Season 3 (days 182-300_ March 2021-July 2021)/209d Samsung Galaxy S III - Underwater world.ogg
190. `FUNTIME`	Season 4 (days 301-present_ July 2021-present)/339d LG CE110 - Fun-Time.mp3
191. `EPUZZLE`	Season 2 (days 92-181_ December 2020-March 2021)/159d Yamaha SMAF (downloadable) - Generate Puzzle.flac
192. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
193. `OLAQDOT`	Season 4 (days 301-present_ July 2021-present)/385n Motorola Q - Dot.mp3
194. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3
195. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
196. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
197. `INGTO~4`	Season 3 (days 182-300_ March 2021-July 2021)/266n Nokia Audio themes - Jazz ringtone.aac
198. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
199. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
200. `0NUANCE`	Season 2 (days 92-181_ December 2020-March 2021)/97n Nokia 6270 - Nuance.aac
201. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav
202. `NGARING`	Season 2 (days 92-181_ December 2020-March 2021)/172n Samsung Galaxy Advance - Ring A Ring.ogg
203. `UNLEASH`	Season 2 (days 92-181_ December 2020-March 2021)/110d Motorola Q9c - Unleash.mp3
204. `9FRIDAY`	Season 1 (days 1-91_ September 2020-December 2020)/39 Samsung Galaxy S9 - Friday.ogg
205. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
206. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
207. `LITHAZE`	Season 4 (days 301-present_ July 2021-present)/338d Motorola E815 - Moonlit Haze.wav
208. `VOLTAGE`	Season 1 (days 1-91_ September 2020-December 2020)/59 BenQ-Siemens S68 - High Voltage.wav
209. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
210. `0VISION`	Season 3 (days 182-300_ March 2021-July 2021)/233d Windows Mobile 6.0 - Vision.wma
211. `OVIOLET`	Season 3 (days 182-300_ March 2021-July 2021)/271d Nokia Oro - Violet.aac
212. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
213. `HANGING`	Season 4 (days 301-present_ July 2021-present)/315d Motorola A3100 - Hanging.mp3
214. `CHINESE`	Season 3 (days 182-300_ March 2021-July 2021)/206n1 Nokia Tune Remake Audiodraft Sound Design Contest - NOKIA CHINESE.mp3
215. `IGHTIDE`	Season 4 (days 301-present_ July 2021-present)/373d Samsung Galaxy S III - High tide.ogg
216. `IDPLAYA`	Season 1 (days 1-91_ September 2020-December 2020)/41 Android - Playa.wav
217. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
218. `EPOLISH`	Season 3 (days 182-300_ March 2021-July 2021)/265d HTC Ozone - Polish.wma
219. `HOLIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/116n Samsung Galaxy S8 - Holiday.ogg
220. `GHTLI~1`	Season 3 (days 182-300_ March 2021-July 2021)/220n Motorola E365 - Night Life.wav
221. `OVATION`	Season 2 (days 92-181_ December 2020-March 2021)/127d HTC E-Club Ring Tone Pack 1 - HTC Innovation.MP3
222. `TENIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/203n Sony Ericsson T700 - Late night.m4a
223. `TJINGLE`	Season 1 (days 1-91_ September 2020-December 2020)/62 T-Jingle (2006).mp3
224. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
225. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
226. `EDUBLIN`	Season 4 (days 301-present_ July 2021-present)/363d T-Mobile Sidekick Slide - Dublin.mp3
227. `GTONE17`	Season 3 (days 182-300_ March 2021-July 2021)/277n LG enV2 - Ringtone 17.wav
228. `310KICK`	Season 4 (days 301-present_ July 2021-present)/340d Nokia 3310 - Kick.wma
229. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
230. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
231. `IGSOLAR`	Season 3 (days 182-300_ March 2021-July 2021)/182d Motorola M702ig - Solar.3gp
232. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
233. `NCOFIRE`	Season 2 (days 92-181_ December 2020-March 2021)/158d Windows Mobile Ring Tone Pack 2 - Flamenco Fire.wma
234. `LLREMIX`	Season 1 (days 1-91_ September 2020-December 2020)/63 Discord - Incoming Call (Remix).mp3
235. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
236. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
237. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
238. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
239. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
240. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
241. `000MOON`	Season 4 (days 301-present_ July 2021-present)/391d Motorola A1000 - Moon.mp3
242. `LLBLEND`	Season 2 (days 92-181_ December 2020-March 2021)/152d Android 1.0 - Curve Ball Blend.ogg
243. `97BEATS`	Season 1 (days 1-91_ September 2020-December 2020)/33 Motorola V197 - Beats.mp3
244. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
245. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
246. `NE7PURE`	Season 1 (days 1-91_ September 2020-December 2020)/08 Windows Phone 7 - Pure.wma
247. `RE2KICK`	Season 4 (days 301-present_ July 2021-present)/359n Motorola ROKR E2 - Kick.mp3
248. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
249. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
250. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
251. `IDHENRY`	Season 4 (days 301-present_ July 2021-present)/344n Gionee GN305 - Horrid Henry.mp3
252. `GTRICKS`	Season 3 (days 182-300_ March 2021-July 2021)/267n Nokia 6101b - Playing tricks.mp3
253. `ITRAILS`	Season 4 (days 301-present_ July 2021-present)/378n Nokia 5140i - Trails.wma
254. `START~3`	Season 4 (days 301-present_ July 2021-present)/380dh Ubuntu 6.10 - Startup.wav

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
