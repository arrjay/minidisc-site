---
title: BANGRS1
date: "2021-12-15"
---

# BANGRS1

## Summary
one of 7 discs mostly randomly generated from
[Ringtone Bangers](https://twitter.com/ringtonebangers)
clips as they have uploaded to google drive. track layout determined via
script. recorded in SP MONO.

the first track was hardwired to be the classic Nokia tone.

the intent of this disc was to push a minidisc session to the limits for
track capacity and title naming. at 254 tracks, that gives each track seven
characters to hold the track name. track 0 is actually the free block map,
and the disc title...

## Tracks
1. [`INGTONE`	Nokia 3310 - Classic Monophonic RINGTONE (2017).mp4](https://www.youtube.com/watch?v=pe1ZXh5_wk4)
2. `BEANICE`	Season 2 (days 92-181_ December 2020-March 2021)/101d Android 1.0 - Caribbean Ice.ogg
3. `NODANCE`	Season 3 (days 182-300_ March 2021-July 2021)/192d Samsung SGH-P250 - Techno dance.mp3
4. `5ROCKME`	Season 3 (days 182-300_ March 2021-July 2021)/235n Motorola ROKR EM35 - Rock Me.mp3
5. `ROOVING`	Season 3 (days 182-300_ March 2021-July 2021)/227d Motorola M702ig - Grooving.3gp
6. `ECLOUDS`	Season 4 (days 301-present_ July 2021-present)/310d Sony Ericsson P1i - Into the clouds.mp3
7. `EBOTTLE`	Season 2 (days 92-181_ December 2020-March 2021)/153n Samsung Galaxy Chat - Wine bottle.ogg
8. `THEEDGE`	Season 3 (days 182-300_ March 2021-July 2021)/246d Motorola E1070 - Off The Edge.mp3
9. `0GROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/149d Motorola A1000 - Groove.mp3
10. `T_TTONE`	Season 2 (days 92-181_ December 2020-March 2021)/130n AT_T - AT_T Tone.wav
11. `OCOLATE`	Season 2 (days 92-181_ December 2020-March 2021)/115n Windows Mobile Ring Tone Pack 3 - Hot Chocolate.wma
12. `OCKTAIL`	Season 2 (days 92-181_ December 2020-March 2021)/142n Motorola RAZR V3i - Cocktail.mp3
13. `LLYDOIT`	Season 2 (days 92-181_ December 2020-March 2021)/109n BenQ-Siemens S68 - You really do it.wav
14. `EFRIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/144d HTC Ozone - Friday.wma
15. `TORYLAP`	Season 4 (days 301-present_ July 2021-present)/326n Google Sounds 2.2 - Victory Lap.ogg
16. `20DJPOP`	Season 1 (days 1-91_ September 2020-December 2020)/52 Alcatel OT-5020D - JPop.mp3
17. `NIVERSE`	Season 1 (days 1-91_ September 2020-December 2020)/45 Motorola SLVR L6g (Chinese version) - Little Universe.mp3
18. `ECIRCUS`	Season 4 (days 301-present_ July 2021-present)/362n Motorola A3100 - Space Circus.mp3
19. `EADNEON`	Season 1 (days 1-91_ September 2020-December 2020)/79 Android 2.3 Gingerbread - Neon.wav
20. `Q9HGLOW`	Season 4 (days 301-present_ July 2021-present)/357n Motorola Q9h - Glow.mp3
21. `ANDWINE`	Season 2 (days 92-181_ December 2020-March 2021)/108n LG KG280 - Cigar And Wine.flac
22. `OCKSOLO`	Season 4 (days 301-present_ July 2021-present)/364d Motorola W180 - Rock Solo.wav
23. `CICONIC`	Season 3 (days 182-300_ March 2021-July 2021)/278d Motorola Q9c - Iconic.mp3
24. `0LOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/281d Motorola A1000 - Lounge.mp3
25. `AYNIGHT`	Season 2 (days 92-181_ December 2020-March 2021)/152n Motorola E398 - Saturday night.mp3
26. `DEMO044`	Season 3 (days 182-300_ March 2021-July 2021)/244d Yamaha SMAF (downloadable) - MA Sound Demo044.wav
27. `00BC110`	Season 2 (days 92-181_ December 2020-March 2021)/142d Nokia 8800 - Bc110.aac
28. `0BRUNCH`	Season 3 (days 182-300_ March 2021-July 2021)/208n Sony Ericsson T700 - Brunch.m4a
29. `DESTINY`	Season 2 (days 92-181_ December 2020-March 2021)/111n Nokia 6681 - Destiny.flac
30. `HEHEIST`	Season 4 (days 301-present_ July 2021-present)/381n Windows Mobile Ring Tone Pack 7 - The Heist.wma
31. `GFUSION`	Season 3 (days 182-300_ March 2021-July 2021)/209n Motorola M702ig - Fusion.3gp
32. `PLUCKER`	Season 2 (days 92-181_ December 2020-March 2021)/123n Android 1.0 - Beat Plucker.ogg
33. `8COFFEE`	Season 3 (days 182-300_ March 2021-July 2021)/236n Samsung Galaxy S8 - Coffee.ogg
34. `WFLAKES`	Season 2 (days 92-181_ December 2020-March 2021)/115d Nokia 6681 - Snowflakes.flac
35. `0ILATIN`	Season 2 (days 92-181_ December 2020-March 2021)/92d Sony Ericsson K800i - Latin.mp3
36. `ITCOMES`	Season 3 (days 182-300_ March 2021-July 2021)/268d Samsung SGH-i400 - Here it comes.mp3
37. `SCRAPER`	Season 2 (days 92-181_ December 2020-March 2021)/98n Samsung Galaxy S8 - Skyscraper.ogg
38. `ALKAWAY`	Season 3 (days 182-300_ March 2021-July 2021)/258d Nokia N85 - Walk away.aac
39. `TICTONE`	Season 1 (days 1-91_ September 2020-December 2020)/28 Samsung Galaxy S5 - Mystic Tone.ogg
40. `IRDLOOP`	Season 2 (days 92-181_ December 2020-March 2021)/164n Android 1.0 - Bird Loop.ogg
41. `SODAPOP`	Season 3 (days 182-300_ March 2021-July 2021)/248d Nokia 6300 (newer firmware) - Soda pop.aac
42. `LYGHOST`	Season 4 (days 301-present_ July 2021-present)/370n Android 0.9 - Friendly Ghost.ogg
43. `LESTONE`	Season 1 (days 1-91_ September 2020-December 2020)/80 HTC Sense 1.0 - Cobblestone.mp3
44. `EDUBLIN`	Season 4 (days 301-present_ July 2021-present)/363d T-Mobile Sidekick Slide - Dublin.mp3
45. `2RHODES`	Season 2 (days 92-181_ December 2020-March 2021)/95n HTC E-Club Ring Tone Pack 2 - Rhodes.MP3
46. `UNSHINE`	Season 4 (days 301-present_ July 2021-present)/368d Samsung Galaxy Note 7 - Sunshine.ogg
47. `RASONIC`	Season 1 (days 1-91_ September 2020-December 2020)/83 Motorola E398 - Ultra Sonic.mp3
48. `FASHION`	Season 2 (days 92-181_ December 2020-March 2021)/104n Motorola RAZR V3i - Fashion.mp3
49. `GETAWAY`	Season 2 (days 92-181_ December 2020-March 2021)/161d Google Pixel 4 - Getaway.ogg
50. `GONAVIS`	Season 3 (days 182-300_ March 2021-July 2021)/202n Android 2.3 Gingerbread - Argo Navis.ogg
51. `ILLASKY`	Season 1 (days 1-91_ September 2020-December 2020)/54 LG KG240 - Vanilla Sky.flac
52. `H1OHRID`	Season 1 (days 1-91_ September 2020-December 2020)/31 Essential PH-1 - Ohrid.ogg
53. `LKCYCLE`	Season 4 (days 301-present_ July 2021-present)/341d Maxo Phone Tones Pack #5 - Walk Cycle.mp3
54. `JUPITER`	Season 3 (days 182-300_ March 2021-July 2021)/270d Alcatel OT-660 - Jupiter.mp3
55. `KCHIMER`	Season 1 (days 1-91_ September 2020-December 2020)/71 Windows Mobile Ring Tone Pack - Chimer.wma
56. `NGARING`	Season 2 (days 92-181_ December 2020-March 2021)/172n Samsung Galaxy Advance - Ring A Ring.ogg
57. `70TWIRL`	Season 2 (days 92-181_ December 2020-March 2021)/150n Motorola E1070 - Twirl.mp3
58. `LEFEVER`	Season 4 (days 301-present_ July 2021-present)/355d LG KG800 Chocolate - Jungle Fever.wav
59. `HESTAGE`	Season 2 (days 92-181_ December 2020-March 2021)/158n Samsung Galaxy S6 - On the Stage.ogg
60. `HANGING`	Season 4 (days 301-present_ July 2021-present)/315d Motorola A3100 - Hanging.mp3
61. `TENIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/203n Sony Ericsson T700 - Late night.m4a
62. `ORGANIC`	Season 3 (days 182-300_ March 2021-July 2021)/191d HTC Touch - Organic.wma
63. `BYWALTZ`	Season 3 (days 182-300_ March 2021-July 2021)/206n3 Samsung Galaxy S20 - Baby Waltz.ogg
64. `LLBLEND`	Season 2 (days 92-181_ December 2020-March 2021)/152d Android 1.0 - Curve Ball Blend.ogg
65. `K3SHINE`	Season 2 (days 92-181_ December 2020-March 2021)/164d Motorola KRZR K3 - Shine.mp3
66. `3STREET`	Season 3 (days 182-300_ March 2021-July 2021)/227n Maxo PHONE TONES PACK #3 - Street.mp3
67. `ASTPACE`	Season 2 (days 92-181_ December 2020-March 2021)/155d Motorola Q9m - Fast Pace.mp3
68. `INGTO~1`	Season 4 (days 301-present_ July 2021-present)/319d Nokia Audio themes - Golf ringtone.aac
69. `CHINESE`	Season 3 (days 182-300_ March 2021-July 2021)/206n1 Nokia Tune Remake Audiodraft Sound Design Contest - NOKIA CHINESE.mp3
70. `ERATION`	Season 2 (days 92-181_ December 2020-March 2021)/170n Samsung Galaxy S III Mini - U generation.ogg
71. `BOOKEM_`	Season 2 (days 92-181_ December 2020-March 2021)/143n HTC HD7 - Book Em_.mp3
72. `NFORMER`	Season 3 (days 182-300_ March 2021-July 2021)/247n HTC One M9 - Informer.mp3
73. `ONLIGHT`	Season 3 (days 182-300_ March 2021-July 2021)/207n HTC Droid DNA - Moon Light.mp3
74. `INGTO~2`	Season 3 (days 182-300_ March 2021-July 2021)/266n Nokia Audio themes - Jazz ringtone.aac
75. `TARTUP3`	Season 4 (days 301-present_ July 2021-present)/380dd KDE 3.3 - Startup 3.ogg
76. `ENALINE`	Season 3 (days 182-300_ March 2021-July 2021)/195d Motorola KRZR K1 - Adrenaline.mp3
77. `GHTLIFE`	Season 4 (days 301-present_ July 2021-present)/308n GFive G9 - Night Life.ogg
78. `ICDISCO`	Season 4 (days 301-present_ July 2021-present)/390d Microsoft Office clipart - Generic Disco.wav
79. `RETLIFE`	Season 2 (days 92-181_ December 2020-March 2021)/166d Samsung SGH-F480 Tocco - Secret Life.mp3
80. `ERIMENT`	Season 4 (days 301-present_ July 2021-present)/347n Nokia N9 - Noise experiment.mp3
81. `OOKBACK`	Season 3 (days 182-300_ March 2021-July 2021)/185n Samsung B5702 DuoS - Look Back.mp3
82. `OADTRIP`	Season 2 (days 92-181_ December 2020-March 2021)/146d Android 1.5 Cupcake - Road Trip.wav
83. `OULMATE`	Season 4 (days 301-present_ July 2021-present)/351n Samsung SGH-D980 DUOS - Melody5 (Soul mate).wav
84. `SEVILLE`	Season 3 (days 182-300_ March 2021-July 2021)/226d Android 1.5 Cupcake - Seville.wav
85. `LATINUM`	Season 4 (days 301-present_ July 2021-present)/325d Android 4.0 Ice Cream Sandwich - Platinum.wav
86. `INAMASK`	Season 4 (days 301-present_ July 2021-present)/367d Motorola V191 (China) - Mask.mp3
87. `DREAMER`	Season 2 (days 92-181_ December 2020-March 2021)/162d HTC 8-Bit Sound Set - Cloud Dreamer.mp3
88. `SIONMA3`	Season 4 (days 301-present_ July 2021-present)/374d Panasonic X300 - Fusion ma-3.wav
89. `RALPARK`	Season 3 (days 182-300_ March 2021-July 2021)/224d LG Sentio - Central Park.mp3
90. `ISTFLOW`	Season 4 (days 301-present_ July 2021-present)/348n Samsung Ch@t 335 - Moist flow.mp3
91. `KIDRIVE`	Season 4 (days 301-present_ July 2021-present)/337n HTC One X - Alki Drive.mp3
92. `CATWALK`	Season 3 (days 182-300_ March 2021-July 2021)/184n Nokia 5300 - Catwalk.aac
93. `THATWAY`	Season 2 (days 92-181_ December 2020-March 2021)/145n Nokia N73 - Stay that way.aac
94. `HORIZON`	Season 2 (days 92-181_ December 2020-March 2021)/128n Samsung Galaxy S II - Over the horizon.ogg
95. `310KICK`	Season 4 (days 301-present_ July 2021-present)/340d Nokia 3310 - Kick.wma
96. `SEASIDE`	Season 1 (days 1-91_ September 2020-December 2020)/26 iOS 7 - By The Seaside.m4r
97. `NEWYEAR`	Season 2 (days 92-181_ December 2020-March 2021)/122n Windows Mobile (downloadable) - Sweet New Year.wma
98. `_SIGNAL`	Season 3 (days 182-300_ March 2021-July 2021)/199n Kyocera Kona - Poppin_ Signal.ogg
99. `ERWORLD`	Season 3 (days 182-300_ March 2021-July 2021)/209d Samsung Galaxy S III - Underwater world.ogg
100. `YLOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/213n Android 1.0 - Loopy Lounge.ogg
101. `ACKDIVE`	Season 3 (days 182-300_ March 2021-July 2021)/230n Samsung Jack - Dive.mp3
102. `GHTCLUB`	Season 3 (days 182-300_ March 2021-July 2021)/262n Motorola ROKR Z6 - Nightclub.mp3
103. `OLAMOTO`	Season 2 (days 92-181_ December 2020-March 2021)/127n Motorola - Moto.mp3
104. `0PLANET`	Season 4 (days 301-present_ July 2021-present)/339n Samsung Galaxy S10 - Planet.ogg
105. `0GLOWHQ`	Season 2 (days 92-181_ December 2020-March 2021)/176d Nokia 7380 - Glow (HQ).wav
106. `LLREMIX`	Season 1 (days 1-91_ September 2020-December 2020)/63 Discord - Incoming Call (Remix).mp3
107. `ANORIFF`	Season 3 (days 182-300_ March 2021-July 2021)/238d iPhone - Piano Riff.m4a
108. `LENUEVO`	Season 2 (days 92-181_ December 2020-March 2021)/174n Windows Mobile (downloadable) - Nuevo.wma
109. `6CANVAS`	Season 3 (days 182-300_ March 2021-July 2021)/243n Nokia E66 - Canvas.aac
110. `YS8AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/198n Samsung Galaxy S8 - Aqua.ogg
111. `RELAXED`	Season 4 (days 301-present_ July 2021-present)/343d Sony Ericsson G705 - Relaxed.m4a
112. `6WAHWAH`	Season 3 (days 182-300_ March 2021-July 2021)/287n Nokia 2626 - Wah wah.mp3
113. `DEMO021`	Season 3 (days 182-300_ March 2021-July 2021)/293d Yamaha SMAF (downloadable) - MA Sound Demo021.wav
114. `REAMYOU`	Season 4 (days 301-present_ July 2021-present)/356d Samsung SGH-i700 - Dream You.wav
115. `LSTREET`	Season 4 (days 301-present_ July 2021-present)/332d Sony Ericsson P1i - Hill street.mp3
116. `OS7SILK`	Season 1 (days 1-91_ September 2020-December 2020)/76 iOS 7 - Silk.m4r
117. `TIMEPAD`	Season 4 (days 301-present_ July 2021-present)/358d Alcatel OT-660 - Time Pad.mp3
118. `00BELL6`	Season 1 (days 1-91_ September 2020-December 2020)/50 Samsung SGH-X100 - Bell 6.flac
119. `NRAMBLE`	Season 4 (days 301-present_ July 2021-present)/331d Alcatel OT-660 - Urban Ramble.mp3
120. `GHSCORE`	Season 2 (days 92-181_ December 2020-March 2021)/151d Windows Mobile Extended Audio Pack 1 - High Score.wma
121. `USRETRO`	Season 4 (days 301-present_ July 2021-present)/378d Vestel Venus - Retro.ogg
122. `QBOOGIE`	Season 4 (days 301-present_ July 2021-present)/383n Windows Mobile (downloadable) - BBQ Boogie.wma
123. `SCOVERY`	Season 2 (days 92-181_ December 2020-March 2021)/148n Nokia 8600 - Discovery.aac
124. `DEMO043`	Season 3 (days 182-300_ March 2021-July 2021)/221d Yamaha SMAF (downloadable) - MA Sound Demo043.wav
125. `BAGITUP`	Season 1 (days 1-91_ September 2020-December 2020)/11 BenQ-Siemens S68 - Bag it up.wav
126. `ITRAILS`	Season 4 (days 301-present_ July 2021-present)/378n Nokia 5140i - Trails.wma
127. `TTAHAVE`	Season 3 (days 182-300_ March 2021-July 2021)/294n Nokia 7370 - Gotta have.aac
128. `CLASSIC`	Season 2 (days 92-181_ December 2020-March 2021)/132n Windows Phone 8 - Classic.wma
129. `REDIBLE`	Season 3 (days 182-300_ March 2021-July 2021)/280d Nokia 6270 - Elecredible.aac
130. `HICBABA`	Season 4 (days 301-present_ July 2021-present)/312n Samsung Galaxy Pocket - Chic baba.ogg
131. `REVERIE`	Season 2 (days 92-181_ December 2020-March 2021)/145d Samsung Ego - Reverie.mp3
132. `LOWFISH`	Season 3 (days 182-300_ March 2021-July 2021)/272n LG KG240 - Blowfish.wav
133. `ARNIVAL`	Season 3 (days 182-300_ March 2021-July 2021)/284d Samsung Galaxy S20 - Carnival.ogg
134. `80IDAWN`	Season 1 (days 1-91_ September 2020-December 2020)/48 Sony Ericsson W380i - Dawn.m4a
135. `USCIOUS`	Season 3 (days 182-300_ March 2021-July 2021)/188n Motorola W490 - Luscious.mp3
136. `ISLANDS`	Season 3 (days 182-300_ March 2021-July 2021)/268n Motorola A920 - The Islands.mp3
137. `CGIMLET`	Season 4 (days 301-present_ July 2021-present)/331n HTC Magic - Gimlet.mp3
138. `RW5COOL`	Season 3 (days 182-300_ March 2021-July 2021)/217n Motorola ROKR W5 - Cool.mp3
139. `NKY_ALL`	Season 4 (days 301-present_ July 2021-present)/302d Android 1.5 Cupcake - Funk Y_all.wav
140. `NICBOOM`	Season 2 (days 92-181_ December 2020-March 2021)/97d Samsung Gravity SGH-T456 - Sonicboom.mp3
141. `TERNOON`	Season 2 (days 92-181_ December 2020-March 2021)/135n LG KG320 - Lazy Afternoon.flac
142. `IDPLAYA`	Season 1 (days 1-91_ September 2020-December 2020)/41 Android - Playa.wav
143. `MINATED`	Season 1 (days 1-91_ September 2020-December 2020)/22 Android - Terminated.ogg
144. `TARTUP2`	Season 4 (days 301-present_ July 2021-present)/380dc KDE 3.3 - Startup 2.ogg
145. `60GREEN`	Season 4 (days 301-present_ July 2021-present)/313d Alcatel OT-660 - Green.mp3
146. `CEPTRUM`	Season 3 (days 182-300_ March 2021-July 2021)/265n Android 2.3 Gingerbread - Sceptrum.ogg
147. `TELLITE`	Season 2 (days 92-181_ December 2020-March 2021)/94n Samsung Galaxy S10 - Satellite.ogg
148. `0SOREAL`	Season 3 (days 182-300_ March 2021-July 2021)/245n LG KG280 - So Real.wav
149. `55BRILL`	Season 3 (days 182-300_ March 2021-July 2021)/257d Nokia E55 - Brill.aac
150. `CENSION`	Season 1 (days 1-91_ September 2020-December 2020)/27 Motorola RIZR Z3 - Ascension.mp3
151. `STARTUP`	Season 4 (days 301-present_ July 2021-present)/380de Mandriva Linux 2006 - Startup.wav
152. `98FLUID`	Season 3 (days 182-300_ March 2021-July 2021)/260d Motorola E398 - Fluid.mp3
153. `RBREATH`	Season 3 (days 182-300_ March 2021-July 2021)/212d Samsung Ego - Hold your breath.mp3
154. `THEDARK`	Season 4 (days 301-present_ July 2021-present)/358n Samsung SGH-Z130 - Dancing in the Dark.mp3
155. `ORSOLAR`	Season 2 (days 92-181_ December 2020-March 2021)/155n Nokia 9500 Communicator - Solar.aac
156. `SAFFRON`	Season 2 (days 92-181_ December 2020-March 2021)/154d HTC Ozone - Saffron.wma
157. `BERBAND`	Season 1 (days 1-91_ September 2020-December 2020)/38 Windows Mobile (downloadable) - Rubber Band.wma
158. `TJINGLE`	Season 1 (days 1-91_ September 2020-December 2020)/62 T-Jingle (2006).mp3
159. `2PEPPER`	Season 3 (days 182-300_ March 2021-July 2021)/205n Nokia N72 - Pepper.aac
160. `E10AQUA`	Season 3 (days 182-300_ March 2021-July 2021)/213d HTC Sense 1.0 - Aqua.mp3
161. `EDITION`	Season 3 (days 182-300_ March 2021-July 2021)/195n Android 1.5 Cupcake - Champagne Edition.wav
162. `OFORION`	Season 2 (days 92-181_ December 2020-March 2021)/169d BenQ-Siemens E71 - Bells of Orion.wav
163. `DYNAMIC`	Season 3 (days 182-300_ March 2021-July 2021)/224n Motorola RAZR V3xx - Dynamic.mp3
164. `CLOUNGE`	Season 4 (days 301-present_ July 2021-present)/316n HTC Magic - Lounge.mp3
165. `SURGENT`	Season 4 (days 301-present_ July 2021-present)/354n Sonic Logos, Links and Beds (Album 2) - It_s Urgent.mp3
166. `WFLAK~1`	Season 2 (days 92-181_ December 2020-March 2021)/113d Windows Mobile Ring Tone Pack 3 - Snow Flakes.wma
167. `START~1`	Season 4 (days 301-present_ July 2021-present)/380dg Ubuntu 4.10 - Startup.wav
168. `IGHTIDE`	Season 4 (days 301-present_ July 2021-present)/373d Samsung Galaxy S III - High tide.ogg
169. `ETSTYLE`	Season 4 (days 301-present_ July 2021-present)/380n Motorola W490 - Street Style.mp3
170. `KYSHORT`	Season 3 (days 182-300_ March 2021-July 2021)/251n Motorola RAZR V3 - Spooky (short).flac
171. `ESSENCE`	Season 3 (days 182-300_ March 2021-July 2021)/248n Motorola V551 - Essence.flac
172. `ENDANCE`	Season 4 (days 301-present_ July 2021-present)/317n HP iPAQ Glisten - Dance.mp3
173. `EPOLISH`	Season 3 (days 182-300_ March 2021-July 2021)/265d HTC Ozone - Polish.wma
174. `3XPERIA`	Season 1 (days 1-91_ September 2020-December 2020)/91 Sony Xperia Z3 - xperia.ogg
175. `IGSOLAR`	Season 3 (days 182-300_ March 2021-July 2021)/182d Motorola M702ig - Solar.3gp
176. `TARTUP1`	Season 4 (days 301-present_ July 2021-present)/380db KDE 3.3 - Startup 1.ogg
177. `LIDSODA`	Season 2 (days 92-181_ December 2020-March 2021)/107d Yamaha SMAF (downloadable) - Solid Soda.flac
178. `OGREMIX`	Season 3 (days 182-300_ March 2021-July 2021)/273d Jamster - Crazy Frog Remix.mpg
179. `ICKITUP`	Season 2 (days 92-181_ December 2020-March 2021)/163n Samsung Galaxy S9 - Pick It Up.ogg
180. `LOTHEME`	Season 3 (days 182-300_ March 2021-July 2021)/223n Windows Mobile (downloadable) - Halo Theme.mp3
181. `RGOTCHA`	Season 3 (days 182-300_ March 2021-July 2021)/219d Android 2.0 Eclair - Gotcha.wav
182. `NCOFIRE`	Season 2 (days 92-181_ December 2020-March 2021)/158d Windows Mobile Ring Tone Pack 2 - Flamenco Fire.wma
183. `THETONE`	Season 4 (days 301-present_ July 2021-present)/306n Android 0.9 - Romancing The Tone.ogg
184. `EGROOVE`	Season 2 (days 92-181_ December 2020-March 2021)/105n Nokia 7270 - Re-groove.aac
185. `370CLUB`	Season 2 (days 92-181_ December 2020-March 2021)/136d Nokia 7370 - Club.3gp
186. `0NUANCE`	Season 2 (days 92-181_ December 2020-March 2021)/97n Nokia 6270 - Nuance.aac
187. `HRIDING`	Season 3 (days 182-300_ March 2021-July 2021)/288n Samsung Ego - Smooth riding.mp3
188. `CHRONOS`	Season 1 (days 1-91_ September 2020-December 2020)/53 Motorola A1000 - Chronos.mp3
189. `EANKUMA`	Season 3 (days 182-300_ March 2021-July 2021)/297n Android 4.2 Jelly Bean - Kuma.ogg
190. `Z3TEMPO`	Season 3 (days 182-300_ March 2021-July 2021)/201d Motorola RIZR Z3 - Tempo.mp3
191. `RADIATE`	Season 2 (days 92-181_ December 2020-March 2021)/149n iOS 7 - Radiate.m4r
192. `IMETREE`	Season 1 (days 1-91_ September 2020-December 2020)/90 Nokia 6680 - Lime tree.aac
193. `ANDMILK`	Season 2 (days 92-181_ December 2020-March 2021)/112d Windows Mobile Ring Tone Pack 8 - Cookies And Milk.wma
194. `IGHTWAY`	Season 3 (days 182-300_ March 2021-July 2021)/263n Yamaha SMAF (downloadable) - Straight Way.wav
195. `SONMARS`	Season 4 (days 301-present_ July 2021-present)/374n T-Mobile Sidekick 3 - Chimps on Mars.mp3
196. `1RISING`	Season 3 (days 182-300_ March 2021-July 2021)/204d Windows Mobile Extended Audio Pack 1 - Rising.wma
197. `6LIQUID`	Season 3 (days 182-300_ March 2021-July 2021)/183n Motorola SLVR L6 - Liquid.mp3
198. `VETHEME`	Season 4 (days 301-present_ July 2021-present)/320d Motorola A1000 CD - Love Theme.mp3
199. `SOLARIS`	Season 3 (days 182-300_ March 2021-July 2021)/187d HTC Ozone - Solaris.wma
200. `ERDISCO`	Season 2 (days 92-181_ December 2020-March 2021)/140d Samsung Galaxy S20 - Roller Disco.ogg
201. `EPUZZLE`	Season 2 (days 92-181_ December 2020-March 2021)/159d Yamaha SMAF (downloadable) - Generate Puzzle.flac
202. `DWESTST`	Season 3 (days 182-300_ March 2021-July 2021)/287d Panasonic G60 - JCD West St..wav
203. `ERSURGE`	Season 2 (days 92-181_ December 2020-March 2021)/134n Motorola E1070 - Power Surge.mp3
204. `ILOUNGE`	Season 3 (days 182-300_ March 2021-July 2021)/211n Motorola RAZR V3i - Lounge.mp3
205. `START~2`	Season 4 (days 301-present_ July 2021-present)/380da KDE 3.0 - Startup.wav
206. `WEETPIE`	Season 4 (days 301-present_ July 2021-present)/334d LG Viewty Smart - Sweet Pie.mp3
207. `FUNTIME`	Season 4 (days 301-present_ July 2021-present)/339d LG CE110 - Fun-Time.mp3
208. `RE2KICK`	Season 4 (days 301-present_ July 2021-present)/359n Motorola ROKR E2 - Kick.mp3
209. `HOLIDAY`	Season 2 (days 92-181_ December 2020-March 2021)/116n Samsung Galaxy S8 - Holiday.ogg
210. `LATIN~1`	Season 3 (days 182-300_ March 2021-July 2021)/272d Motorola Q9m - Platinum.mp3
211. `LLATION`	Season 3 (days 182-300_ March 2021-July 2021)/240n Motorola ROKR EM35 - Oscillation.mp3
212. `AINLINK`	Season 3 (days 182-300_ March 2021-July 2021)/294d Motorola V600 - Chain Link.wav
213. `0SUNSET`	Season 2 (days 92-181_ December 2020-March 2021)/160n Motorola A1000 - Sunset.mp3
214. `LLYWOOD`	Season 3 (days 182-300_ March 2021-July 2021)/208d Android 1.5 Cupcake - Bollywood.wav
215. `DLEAVES`	Season 3 (days 182-300_ March 2021-July 2021)/235d HTC Touch HD - Leaves.wma
216. `LDSTYLE`	Season 2 (days 92-181_ December 2020-March 2021)/96n Motorola RIZR Z3 - Wild Style.mp3
217. `POPXRAY`	Season 4 (days 301-present_ July 2021-present)/382n Alcatel M_POP - X-Ray.mp3
218. `CECRAFT`	Season 3 (days 182-300_ March 2021-July 2021)/221n Samsung Galaxy S7 - Spacecraft.ogg
219. `SEDRIVE`	Season 4 (days 301-present_ July 2021-present)/335d fusoxide_s Ringtones Vol. 1 - House Drive.wav
220. `RO2STEP`	Season 3 (days 182-300_ March 2021-July 2021)/284n HTC Hero - 2 Step.mp3
221. `GTONE01`	Season 1 (days 1-91_ September 2020-December 2020)/42 Windows 7 - Ringtone 01.wma
222. `NEALARM`	Season 4 (days 301-present_ July 2021-present)/328n iPhone - Alarm.m4a
223. `CSLEAZY`	Season 1 (days 1-91_ September 2020-December 2020)/10 Motorola E1000 - Mcsleazy.mp3
224. `GTONE17`	Season 3 (days 182-300_ March 2021-July 2021)/277n LG enV2 - Ringtone 17.wav
225. `DEMO041`	Season 3 (days 182-300_ March 2021-July 2021)/190n Yamaha SMAF (downloadable) - MA Sound Demo041.wav
226. `ROHOUSE`	Season 3 (days 182-300_ March 2021-July 2021)/296d Samsung PC Studio - Euro House.wav
227. `2COVERT`	Season 3 (days 182-300_ March 2021-July 2021)/219n HTC HD2 - Covert.wma
228. `AYDREAM`	Season 3 (days 182-300_ March 2021-July 2021)/262d Windows Mobile Ring Tone Pack 5 - Day Dream.wma
229. `SSWIVIT`	Season 3 (days 182-300_ March 2021-July 2021)/290n Android 2.0 Eclair - Don_ Mess Wiv It.wav
230. `OVATION`	Season 2 (days 92-181_ December 2020-March 2021)/127d HTC E-Club Ring Tone Pack 1 - HTC Innovation.MP3
231. `PDRAGON`	Season 2 (days 92-181_ December 2020-March 2021)/165d Nokia 7710 - Snapdragon.aac
232. `OUNGING`	Season 3 (days 182-300_ March 2021-July 2021)/276d Nokia 7370 - Lounging.wav
233. `URNMEON`	Season 2 (days 92-181_ December 2020-March 2021)/173n LG Lotus - Turn Me On.mp3
234. `SYBREAK`	Season 3 (days 182-300_ March 2021-July 2021)/267d HTC E-Club Ring Tone Pack 2 - Bluesy Break.MP3
235. `ILKYWAY`	Season 2 (days 92-181_ December 2020-March 2021)/180n Android 2.0 Eclair - Silky Way.wav
236. `YLIGHTS`	Season 2 (days 92-181_ December 2020-March 2021)/174d Samsung Galaxy S8 - City Lights.ogg
237. `ELODY12`	Season 2 (days 92-181_ December 2020-March 2021)/124n Doro PhoneEasy 410gsm - Melody 12.mp3
238. `VOLTAGE`	Season 1 (days 1-91_ September 2020-December 2020)/59 BenQ-Siemens S68 - High Voltage.wav
239. `5RUNWAY`	Season 4 (days 301-present_ July 2021-present)/350d Nokia N85 - Runway.wma
240. `RL7FIRE`	Season 2 (days 92-181_ December 2020-March 2021)/100d Motorola SLVR L7 - Fire.m4a
241. `ERSPACE`	Season 2 (days 92-181_ December 2020-March 2021)/175d Nokia 6108 - Hyperspace.flac
242. `START~3`	Season 4 (days 301-present_ July 2021-present)/380dh Ubuntu 6.10 - Startup.wav
243. `YFUTURE`	Season 3 (days 182-300_ March 2021-July 2021)/260n Sony Ericsson Vivaz - X-ray future.m4a
244. `CLAPPER`	Season 4 (days 301-present_ July 2021-present)/346d Nokia N78 - Clapper.mp4
245. `90WAVES`	Season 2 (days 92-181_ December 2020-March 2021)/135d Motorola C390 - Waves.mp3
246. `ERUNWAY`	Season 4 (days 301-present_ July 2021-present)/389n LG dLite - Runway.mp3
247. `LAWLESS`	Season 4 (days 301-present_ July 2021-present)/332n Motorola Q9h - Flawless.mp3
248. `NMYSOUL`	Season 2 (days 92-181_ December 2020-March 2021)/157n Sony Ericsson T700 - In my soul.m4a
249. `0AMINOR`	Season 3 (days 182-300_ March 2021-July 2021)/197n LG S5100 - A minor.wav
250. `ULTTONE`	Season 4 (days 301-present_ July 2021-present)/342n LG Venus - VZW Default Tone.mp3
251. `ISHLINE`	Season 4 (days 301-present_ July 2021-present)/315n Google Sounds 2.2 - Finish Line.ogg
252. `INGTO~3`	Season 3 (days 182-300_ March 2021-July 2021)/251d Nokia Audio themes - Car videoringtone.aac
253. `ISMIRON`	Season 3 (days 182-300_ March 2021-July 2021)/281n Nokia 7500 Prism - Iron.aac
254. `UNLEASH`	Season 2 (days 92-181_ December 2020-March 2021)/110d Motorola Q9c - Unleash.mp3

## Mixing/Sourcing Notes
source to the script is available [HERE](https://gist.github.com/arrjay/3837fb1eca6d87f61d54741f9cf93a8d).
no promises it will run anywhere or is fit for any purpose.

ringtone bangers archives are available via google drive, as linked in
[their tweet](https://twitter.com/ringtonebangers/status/1444785594941550596)
