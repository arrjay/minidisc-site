---
title: "Rolling Stone - Turn It Up"
date: "1994-06-30"
---

# Rolling Stone - Turn It Up

## Summary

A prerecorded MD released as a promotional item with issue 685 of
[Rolling Stone](https://www.rollingstone.com/) magazine. I didn't win
the prize. I didn't even get this at the time, it came with another assortment
of MiniDiscs later.

Most of the actual disc titles are all caps, in format ARTIST "TITLE"
_except_ for k.d. lang. Titles have been reformatted to mixed-case
Title (Artist) here.

## Tracks
1. Without A Trace (Soul Asylum)
2. Divine Hammer (The Breeders)
3. Breaking The Girl (Red Hot Chili Peppers)
4. Precious Things (Tori Amos)
5. These Are Days (10,000 Maniacs)
6. Jimmi Diggin Cats (Digable Planets)
7. Nothingness (Living Colour)
8. Sweet Lullaby (Deep Forest)
9. In Perfect Dreams (k.d. lang)
10. Inside (Toad the Wet Sprocket)
11. Sky Blue and Black (Jackson Browne)
12. Bury My Lovely (October Project)
13. Under My Skin (Dandelion)
14. Mass Appeal (Gangstar)
15. The Great Big No (The Lemonheads)
16. [Promotion Message](https://www.youtube.com/watch?v=EWMjeCgbR2Y)

## Disc Notes
This disc is also catalogued in discogs
[here](https://www.discogs.com/release/3995087-Various-Rolling-Stone-Turn-It-Up).